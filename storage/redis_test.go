package storage

import (
	"gitlab.com/walking4wq/promaction/model"
	"gitlab.com/walking4wq/promaction/util"
	"testing"
)

func TestGoReids_ScanDevice(t *testing.T) {
	url := "redis://localhost:6379"
	rds, err := NewGoRedis(url)
	if err != nil {
		t.Fatalf(err.Error())
	}
	match := "*"
	cnt := 0
	pge := 0
	cnt, pge, err = rds.ScanDevice(match, 0, -1, nil,
		func(key4match, key string, offset int, dev *model.Device, err error) (brk bool) {
			t.Logf("%3d[%s]>%s:%s err:%v", offset, key4match, key, util.Marshal4json(dev), err)
			return
		})
	t.Logf("scan4keys match $prefix/%s device walked %d/%d devices err:%v", match, pge, cnt, err)
}
