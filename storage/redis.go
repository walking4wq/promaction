package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v9"
	"gitlab.com/walking4wq/promaction/model"
	"gitlab.com/walking4wq/promaction/util"
	"path/filepath"
	"sort"
	"time"
)

type GoReids struct {
	clt *redis.Client

	prefix, prefix4dev, prefix4job string
	context.Context
}

func NewGoRedis(url string) (hdl *GoReids, err error) {
	var opts *redis.Options
	opts, err = redis.ParseURL(url)
	if err != nil {
		return
	}
	rdb := redis.NewClient(opts)
	hdl = &GoReids{
		clt: rdb, Context: context.TODO(), prefix: "pma", prefix4dev: "dev", prefix4job: "job",
	}
	return
}

func (this *GoReids) Get(key string) (val string, err error) {
	val, err = this.clt.Get(this.Context, key).Result()
	if err != nil {
		err = fmt.Errorf("get[%s] from redis[%v] err:%v", key, this.clt, err)
	}
	return
}
func (this *GoReids) Set(key string, val interface{}, expiration time.Duration) (err error) {
	err = this.clt.Set(this.Context, key, val, expiration).Err()
	if err != nil {
		err = fmt.Errorf("set[%s,%v,%v] from redis[%v] err:%v", key, val, expiration, this.clt, err)
	}
	return
}
func (this *GoReids) Del(key string) (cnt int64, err error) {
	cnt, err = this.clt.Del(this.Context, key).Result()
	if err != nil {
		err = fmt.Errorf("del[%s] from redis[%v] err:%d,%v", key, this.clt, cnt, err)
	}
	return
}
func (this *GoReids) Scan(cursor uint64, match string, count int64, walk Walk4keys) (err error) {
	var keys []string
	var cur4nxt uint64
	var skip *bool
	for {
		keys, cur4nxt, err = this.clt.Scan(this.Context, cursor, match, count).Result()
		if err != nil {
			err = fmt.Errorf("scan %d match %q count %d err:%v", cursor, match, count, err)
			return
		}
		for i, key := range keys {
			skip = walk(cursor, match, key, i)
			if skip != nil {
				if *skip {
					break
				} else {
					return
				}
			} // continue
		}
		if cur4nxt == 0 {
			return
		} else {
			cursor = cur4nxt
		}
	}
}

func (this *GoReids) Key4dev(code string) (key string) {
	return filepath.Join(this.prefix, this.prefix4dev, code)
}
func (this *GoReids) Key4devJob(prefix string, id4job string) (key string) {
	return filepath.Join(this.prefix, this.prefix4dev, this.prefix4job, prefix, id4job)
}

func (this *GoReids) scan4keys(cursor uint64, match string, count int64) (set4keys util.Set, err error) {
	if count < 3 {
		count = 3
	}
	var keys []string
	var cur4nxt uint64
	for {
		keys, cur4nxt, err = this.clt.Scan(this.Context, cursor, match, count).Result()
		if err != nil {
			err = fmt.Errorf("scan4keys %d match %q count %d err:%v", cursor, match, count, err)
			return
		}
		set4keys = make(util.Set, len(keys))
		for _, key := range keys {
			// ref: https://redis.io/commands/scan/
			// It is up to the application to handle the case of duplicated elements
			set4keys[key] = util.Sizeof0
		}
		if cur4nxt == 0 {
			return
		} else {
			cursor = cur4nxt
		}
	}
}

func (this *GoReids) getDevice(key string) (dev *model.Device, err error) {
	var val string
	val, err = this.Get(key)
	if err != nil {
		err = fmt.Errorf("device %v", err)
		return
	}
	dev = &model.Device{}
	err = json.Unmarshal(util.Slice(val), dev)
	if err != nil {
		err = fmt.Errorf("json.Unmarshal device[%s] err:%v by:%v", key, err, val)
	}
	return
}
func (this *GoReids) GetDevice(code string) (dev *model.Device, err error) {
	return this.getDevice(this.Key4dev(code))
}

func (this *GoReids) ScanDevice(match string, siz4page, idx4page int, asc *bool, walk ScanDeviceWalkFn) (cnt, pge int, err error) {
	key4match := this.Key4dev(match)
	count := int64(siz4page)
	if count < 3 {
		count = 3
	}
	var set4keys util.Set
	set4keys, err = this.scan4keys(0, key4match, count)
	if err != nil {
		err = fmt.Errorf("device match[%s] page[%d/size%d] asc:%v err:%v", match, siz4page, idx4page, asc, err)
		return
	}
	cnt = len(set4keys)
	if cnt < 1 {
		return
	}
	var begin, end int
	begin, end, pge = util.Paging(cnt, siz4page, idx4page)
	if begin >= end {
		return
	}
	keys := make([]string, 0, cnt)
	for key := range set4keys {
		keys = append(keys, key)
	}
	if asc != nil {
		if *asc {
			sort.Strings(keys) // sort.Sort(sort.StringSlice(keys))
		} else {
			sort.Sort(sort.Reverse(sort.StringSlice(keys)))
		}
	}

	var cmds []redis.Cmder // ref: https://redis.uptrace.dev/guide/go-redis-pipelines.html#pipelines
	cmds, err = this.clt.Pipelined(this.Context, func(pipeliner redis.Pipeliner) error {
		//var cmd *redis.StringCmd
		for _, key := range keys[begin:end] {
			//cmd
			_ = pipeliner.Get(this.Context, key)
		}
		return nil
	})
	for idx, cmd := range cmds {
		offset := begin + idx
		var data4dev string
		data4dev, err = cmd.(*redis.StringCmd).Result()
		if err != nil {
			brk := walk(key4match, keys[idx], offset, nil, err)
			if brk {
				break
			}
			continue
		}
		dev := &model.Device{}
		err = json.Unmarshal(util.Slice(data4dev), dev)
		brk := walk(key4match, keys[idx], offset, dev, err)
		if brk {
			break
		}
	}
	//for idx, key := range keys[begin:end] {
	//	offset := begin + idx
	//	var dev *model.Device
	//	dev, err = this.getDevice(key)
	//	brk := walk(key4match, key, offset, dev, err)
	//	if brk {
	//		break
	//	}
	//}
	return
}

func (this *GoReids) ScanJob(prefix string, walk Walk4keys) (err error) {
	key4match := this.Key4devJob(prefix, "*")
	return this.Scan(0, key4match, 512, walk)
}
func (this *GoReids) GetJob(key string) (job *util.Job, err error) {
	var val string
	val, err = this.Get(key)
	if err != nil {
		err = fmt.Errorf("dev job %v", err)
		return
	}
	job = &util.Job{}
	err = json.Unmarshal(util.Slice(val), job)
	if err != nil {
		err = fmt.Errorf("json.Unmarshal device job[%s] err:%v by:%v", key, err, val)
	}
	return
}
func (this *GoReids) SetJob(prefix string, job *util.Job) (err error) {
	var json4data []byte
	json4data, err = json.Marshal(job)
	if err != nil {
		err = fmt.Errorf("json.Marshal device job err:%v by:%v", err, job)
		return
	}
	key := this.Key4devJob(prefix, job.Id)
	return this.Set(key, util.String(json4data), 0)
}
func (this *GoReids) SetJobs(prefix string, jobs []*util.Job) (cnt int, err error) {
	var k2v []string // [][2]string
	var json4data []byte
	var key string
	for i, job := range jobs {
		json4data, err = json.Marshal(job)
		if err != nil {
			err = fmt.Errorf("json.Marshal device jobs[%d] err:%v by:%v", i, err, job)
			return
		}
		key = this.Key4devJob(prefix, job.Id)
		//k2v = append(k2v, [2]string{key, util.String(json4data)})
		k2v = append(k2v, key, util.String(json4data))
	}
	// this.clt.Eval(this.Context, ``, nil, nil)
	scr := redis.NewScript(`
local num_arg = #ARGV
local key = ''
local val = ''
-- local replay = {}
local cnt = 0
for i = 1, num_arg do
  if( i%2 == 1 ) then
    key = ARGV[i]
  else
    val = ARGV[i]
    -- redis.call('SET', key, val)
    local reply = redis.pcall('SET', key, val)
    if reply['err'] ~= nil then
      return redis.error_reply('set ' .. key .. ' ' .. val .. ' err:' .. replay['err'])
    end
    cnt = cnt + 1
  end
end

-- return cnt
return redis.status_reply('' .. cnt)`)
	cnt, err = scr.Run(this.Context, this.clt, nil, k2v).Int()
	return
}
func (this *GoReids) DelJob(prefix string, id4job string) (cnt int64, err error) {
	key := this.Key4devJob(prefix, id4job)
	cnt, err = this.clt.Del(this.Context, key).Result()
	if err != nil {
		err = fmt.Errorf("dev job %v", err)
		return
	}
	return
}

func (this *GoReids) test4syntax() (err error) {
	var key, value string
	this.clt.RPush(this.Context, key, value) // https://redis.io/commands/rpush/

	var src, dest string
	// https://mp.weixin.qq.com/s/_q0bI62iFrG8h-gZ-bCvNQ
	// https://redis.io/commands/lmove/
	this.clt.LMove(this.Context, src, dest, "RIGHT", "LEFT")
	// https://redis.io/docs/manual/transactions/
	// https://redis.uptrace.dev/guide/lua-scripting.html#redis-script
	// https://redis.io/docs/manual/programmability/lua-api/
	scr := redis.NewScript(``)
	rst, err := scr.Run(this.Context, this.clt, nil).Result()
	_ = rst
	// https://redis.io/commands/rename/
	//this.clt.Rename()

	scr4lua := `
local num_arg = #ARGV
local key = ''
local val = ''
-- local replay = {}
local cnt = 0
for i = 1, num_arg do
  if( i%2 == 1 ) then
    key = ARGV[i]
  else
    val = ARGV[i]
    -- redis.call('SET', key, val)
    local reply = redis.pcall('SET', key, val)
    if reply['err'] ~= nil then
      return redis.error_reply('set ' .. key .. ' ' .. val .. ' err:' .. replay['err'])
    end
    cnt = cnt + 1
  end
end

-- return cnt
return redis.status_reply('' .. cnt)`
	scr4lua = `local num_arg = #ARGV\nlocal key = ''\nlocal val = ''\n-- local replay = {}\nlocal cnt = 0\nfor i = 1, num_arg do\n  if( i%2 == 1 ) then\n    key = ARGV[i]\n  else\n    val = ARGV[i]\n    -- redis.call('SET', key, val)\n    local reply = redis.pcall('SET', key, val)\n    if reply['err'] ~= nil then\n      return redis.error_reply('set ' .. key .. ' ' .. val .. ' err:' .. replay['err'])\n    end\n    cnt = cnt + 1\n  end\nend\n\n-- return cnt\nreturn redis.status_reply('' .. cnt)`
	_ = `eval "local num_arg = #ARGV\nlocal key = ''\nlocal val = ''\n-- local replay = {}\nlocal cnt = 0\nfor i = 1, num_arg do\n  if( i%2 == 1 ) then\n    key = ARGV[i]\n  else\n    val = ARGV[i]\n    -- redis.call('SET', key, val)\n    local reply = redis.pcall('SET', key, val)\n    if reply['err'] ~= nil then\n      return redis.error_reply('set ' .. key .. ' ' .. val .. ' err:' .. replay['err'])\n    end\n    cnt = cnt + 1\n  end\nend\n\n-- return cnt\nreturn redis.status_reply('' .. cnt)" 0 key val`
	this.clt.Eval(this.Context, scr4lua, nil, []string{"key", "val"})
	//this.clt.ZScan()
	return
}
