package storage

import (
	"gitlab.com/walking4wq/promaction/model"
	"gitlab.com/walking4wq/promaction/util"
)

type Walk4keys func(cursor uint64, match, key string, offset int) (skip *bool)

type ScanDeviceWalkFn func(key4match, key string, offset int, dev *model.Device, err error) (brk bool)

type Storage interface {
	//Get(key string) (val string, err error)
	//Set(key string, val interface{}, expiration time.Duration) (err error)
	//Scan(cursor uint64, match string, count int64, walk Walk4keys) (err error)

	Key4dev(code string) (key string)
	Key4devJob(prefix string, id4job string) (key string)

	GetDevice(code string) (dev *model.Device, err error)
	ScanDevice(match string, siz4page, idx4page int, asc *bool, walk ScanDeviceWalkFn) (cnt, pge int, err error)

	ScanJob(prefix string, walk Walk4keys) (err error)
	GetJob(key string) (job *util.Job, err error)
	SetJob(prefix string, job *util.Job) (err error)
	SetJobs(prefix string, jobs []*util.Job) (cnt int, err error)
	DelJob(prefix string, id4job string) (cnt int64, err error)
}

func New(url string) (hdl Storage, err error) {
	hdl, err = NewGoRedis(url)
	return
}
