package util

import (
	"fmt"
	"github.com/gorhill/cronexpr"
	"strings"
	"testing"
	"time"
)

// parse4cronexpr0 import cronexpr0 "github.com/hashicorp/cronexpr"
//func parse4cronexpr0(t *testing.T, expr string) (expr4cron *cronexpr0.Expression) {
//	expr4cron, err := cronexpr0.Parse(expr)
//	if err != nil {
//		t.Fatal(fmt.Sprintf("cronexpr0.Parse(%s) err:%v", expr, err))
//	}
//	return
//}
func parse4cronexpr(t *testing.T, expr string) (expr4cron *cronexpr.Expression) {
	expr4cron, err := cronexpr.Parse(expr)
	if err != nil {
		t.Fatal(fmt.Sprintf("cronexpr.Parse(%s) err:%v", expr, err))
	}
	return
}

func TestCronlist_simple(t *testing.T) {
	cron := "30 * * * *"
	expr4keys := strings.Split(cron, Separator)
	t.Log(fmt.Sprintf("strings.Split(%s,%s)", cron, Separator), expr4keys)
	crons, err := NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	if len(offsets) != 1 && offsets[0] != 0 {
		t.Error("offsets error", offsets)
	}

	cron = "0 0/10 * * *" // ?
	crons, err = NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	next, offsets = crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	if len(offsets) != 1 && offsets[0] != 0 {
		t.Error("offsets error", offsets)
	}
	//expr4cron0 := parse4cronexpr0(t, cron)
	//next = expr4cron0.Next(now)
	//t.Log(fmt.Sprintf("cronexpr.Next(%v)", now), next, offsets)
	expr4cron := parse4cronexpr(t, cron)
	next = expr4cron.Next(now)
	t.Log(fmt.Sprintf("cronexpr.Next(%v)", now), next, offsets)

	cron = "0/10 * * * *" // ?
	crons, err = NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	next, offsets = crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	if len(offsets) != 1 && offsets[0] != 0 {
		t.Error("offsets error", offsets)
	}
}

func TestCronlist_loop4next(t *testing.T) {
	cron := "0 * * * *" // "*/5 * * * *" // "*/5 * * * *" is ok too
	crons, err := NewCronList(cron)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	}
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	for i := 0; i < 3; i++ {
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	}

	zone := "America/Los_Angeles"
	loc4pst, err := time.LoadLocation(zone)
	if err != nil {
		t.Errorf("time.LoadLocation(%s) err:%v", zone, err)
	}
	t.Logf("time.LoadLocation(%s) got:[%s]%v", zone, loc4pst.String(), loc4pst)
	next = time.Date(2021, 3, 14, 0, 0, 0, 0, loc4pst)
	next4utc := next.UTC()
	t.Logf("%v=%v", next, next4utc)
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v:%v)", now, now.UTC()), next, offsets, "UTC", next.UTC())
	}
	t.Log("check the UTC cron next")
	next = next4utc
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets, "loc PST", next.In(loc4pst))
	}
	next = time.Date(2021, 11, 7, 0, 0, 0, 0, loc4pst)
	next4utc = next.UTC()
	t.Logf("%v=%v", next, next4utc)
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v:%v)", now, now.UTC()), next, offsets, "UTC", next.UTC())
	}
	t.Log("check the UTC cron next")
	next = next4utc
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets, "loc PST", next.In(loc4pst))
	}

	cron = "0 2 * * *"
	crons, err = NewCronList(cron)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	}
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	next = time.Date(2021, 3, 14, 0, 0, 0, 0, loc4pst)
	t.Logf("%v=%v", next, next4utc)
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v:%v)", now, now.UTC()), next, offsets, "UTC", next.UTC())
	}
	cron = "0 1 * * *"
	crons, err = NewCronList(cron)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	}
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	next = time.Date(2021, 11, 7, 0, 0, 0, 0, loc4pst)
	t.Logf("%v=%v", next, next4utc)
	for i := 0; i < 3; i++ { // 12*24
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v:%v)", now, now.UTC()), next, offsets, "UTC", next.UTC())
	}
}

func TestCronlist_list(t *testing.T) {
	cron := "30 * * * *|0/30 * * * *"
	expr4keys := strings.Split(cron, Separator)
	t.Log(fmt.Sprintf("strings.Split(%s,%s)", cron, Separator), expr4keys)
	crons, err := NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)

	cron = "0/10 * * * *" // "0/10 * * ?"
	crons, err = NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	next, offsets = crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
}

func TestCronlist_times(t *testing.T) {
	trigs := [][3]string{
		{"00:00", "01:00", "1"},
		{"00:00", "01:00", "6"},
		{"00:00", "01:00", "3"},
		{"00:00", "01:00", "2"},
		//{"00:00", "01:00", "7"}, // for cronexpr
		{"00:00", "01:00", "0"}, // for cron
	}
	//
	// .---------------- minute (0 - 59)
	// | .------------- hour (0 - 23)
	// | | .---------- day of month (1 - 31)
	// | | | .------- month (1 - 12) OR jan,feb,mar,apr ...
	// | | | | .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
	// | | | | |
	// * * * * * user-name command to be executed
	sb4cron := strings.Builder{}
	for i, trig := range trigs {
		if i > 0 {
			sb4cron.WriteString("|")
		}
		tm, err := time.Parse("15:04", trig[0])
		if err != nil {
			t.Errorf("time.Parse(\"15:04\",%s) err:%v", trig[0], err)
		}
		sb4cron.WriteString(fmt.Sprintf("%d %d * * %s", tm.Minute(), tm.Hour(), trig[2]))
		sb4cron.WriteString("|")
		tm, err = time.Parse("15:04", trig[1])
		if err != nil {
			t.Errorf("time.Parse(\"15:04\",%s) err:%v", trig[1], err)
		}
		sb4cron.WriteString(fmt.Sprintf("%d %d * * %s", tm.Minute(), tm.Hour(), trig[2]))
	}
	cron := sb4cron.String()
	t.Log(cron)
	crons, err := NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	for i := 0; i < 12; i++ {
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	}

}
func TestCronlist_nexts(t *testing.T) {
	cron := "" +
		"0 0 * * 1|0 2 * * 1|0 2 * * 1|0 7 * * 1|0 21 * * 1|55 23 * * 1|" +
		"0 0 * * 2|0 2 * * 2|0 2 * * 2|0 7 * * 2|0 21 * * 2|55 23 * * 2|" +
		"0 0 * * 3|0 2 * * 3|0 2 * * 3|0 7 * * 3|0 21 * * 3|55 23 * * 3|" +
		"0 0 * * 4|0 2 * * 4|0 2 * * 4|0 7 * * 4|0 21 * * 4|55 23 * * 4|" +
		"0 0 * * 5|0 2 * * 5|0 2 * * 5|0 7 * * 5|0 21 * * 5|55 23 * * 5|" +
		"0 0 * * 6|0 2 * * 6|0 2 * * 6|0 7 * * 6|0 21 * * 6|55 23 * * 6|" +
		//"0 0 * * 7|0 2 * * 7|0 2 * * 7|0 7 * * 7|0 21 * * 7|55 23 * * 7" // for cronexpr
		"0 0 * * 0|0 2 * * 0|0 2 * * 0|0 0 * * 0|0 21 * * 0|55 23 * * 0" // for cron
	crons, err := NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	for i := 0; i < 12; i++ {
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	}
}

func TestCronList_NoNext(t *testing.T) {
	cron := "18 18 2 3 *" // 2019 // 2019-03-02 18:18
	crons, err := NewCronList(cron)
	t.Log(fmt.Sprintf("util.NewCronList(%s)", cron), crons, err)
	if err != nil {
		t.Fatal(fmt.Sprintf("util.NewCronList(%s) return:%v,%v", cron, crons, err))
	}
	now := time.Now()
	next, offsets := crons.Next(now)
	t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	for i := 0; i < 12; i++ {
		now = next
		next, offsets = crons.Next(now)
		t.Log(fmt.Sprintf("crons.Next(%v)", now), next, offsets)
	}

	cron2 := "18 18 2 3 * 2019" // 2019-03-02 18:18
	expr4cron := parse4cronexpr(t, cron2)
	next = expr4cron.Next(now)
	t.Log(fmt.Sprintf("cronexpr.Next(%v)", now), next, offsets)
}

func TestCronList_Next(t *testing.T) {
	data4test := []struct {
		cron string
		args [][2]string
	}{
		{"0 * * * *", [][2]string{
			{"2021-03-14T00:00:00-08:00", "2021-03-14T01:00:00-08:00"},
			{"2021-03-14T01:00:00-08:00", "2021-03-14T03:00:00-07:00"},
			{"2021-03-14T03:00:00-07:00", "2021-03-14T04:00:00-07:00"},

			{"2021-11-07T00:00:00-07:00", "2021-11-07T01:00:00-07:00"},
			{"2021-11-07T01:00:00-07:00", "2021-11-07T01:00:00-08:00"},
			{"2021-11-07T01:00:00-08:00", "2021-11-07T02:00:00-08:00"},
		}},
		{"0 2 * * *", [][2]string{
			{"2021-03-13T00:00:00-08:00", "2021-03-13T02:00:00-08:00"},
			{"2021-03-14T00:00:00-08:00", "2021-03-14T02:00:00-08:00"},
			{"2021-03-14T01:00:00-08:00", "2021-03-14T03:00:00-07:00"},
			{"2021-03-15T01:00:00-07:00", "2021-03-15T02:00:00-07:00"},
		}},
		{"0 2 * * *", [][2]string{
			{"2021-11-06T00:00:00-07:00", "2021-11-06T02:00:00-07:00"},
			{"2021-11-07T00:00:00-07:00", "2021-11-07T02:00:00-07:00"},
			{"2021-11-07T00:00:00-07:00", "2021-11-07T01:00:00-08:00"},
			{"2021-11-08T02:00:00-08:00", "2021-11-09T02:00:00-08:00"},
		}},
	}
	for i, dt := range data4test {
		crons, err := NewCronList(dt.cron)
		if err != nil {
			t.Fatal(fmt.Sprintf("%3d> util.NewCronList(%s) return:%v,%v", i, dt.cron, crons, err))
		} else {
			t.Log(fmt.Sprintf("%3d> util.NewCronList(%s) return:%v,%v", i, dt.cron, crons, err))
		}
		for j, tm := range dt.args {
			curr, err := time.Parse(FMT4MYSQLLOADTMZ, tm[0])
			if err != nil {
				t.Fatal(fmt.Sprintf("%3d>%3d time.Parse(%s,%s) for curr return:%v,%v", i, j, FMT4MYSQLLOADTMZ, tm[0], curr, err))
			}
			next, err := time.Parse(FMT4MYSQLLOADTMZ, tm[1])
			if err != nil {
				t.Fatal(fmt.Sprintf("%3d>%3d time.Parse(%s,%s) for next return:%v,%v", i, j, FMT4MYSQLLOADTMZ, tm[1], next, err))
			}
			rst, lst := crons.Next(curr)
			if len(lst) == 1 && rst.Equal(next) {
				t.Log(fmt.Sprintf("%3d>%3d crons[%v].Next(%v) return:%v==%v", i, j, crons, curr, rst, next))
			} else {
				t.Errorf(fmt.Sprintf("%3d>%3d crons[%v].Next(%v) return:%v!=%v", i, j, crons, curr, rst, next))
			}
		}
	}
}
