package util

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/go-stack/stack"
	log "github.com/inconshreveable/log15"
	"github.com/prometheus/client_golang/prometheus"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Msg struct {
	Id   string          `json:"id,omitempty"`
	Name string          `json:"name"`
	Req  json.RawMessage `json:"req,omitempty"`
	Rsp  json.RawMessage `json:"rsp,omitempty"`
	Data string          `json:"data,omitempty"`
	Code string          `json:"code,omitempty"`
	Info string          `json:"info,omitempty"`

	UnixMilli4exe int64  `json:"um4exe,omitempty"`
	Cached        string `json:"cached,omitempty"`
}

func (this *Msg) Key4req() (key string) {
	if this == nil {
		return
	}
	data := append(this.Req, Slice(this.Name)...)
	bts := sha256.Sum256(data)
	return hex.EncodeToString(bts[:])
}

//type CachedMsg struct {
//	*Msg
//	time.Time `json:"-"`
//	Timeout   string `json:"timeout,omitempty"`
//}
//type MsgCache map[string]CachedMsg

//var ddeCallDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
//	Name:    "dde_call_duration_seconds",
//	Help:    "Duration of call dde service.",
//	Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 3, 4, 5, 7, 10}, // prometheus.DefBuckets,
//}, []string{"id"})

type Service func(req *Msg, env ...interface{}) (rsp *Msg, err error)
type Services struct {
	Mapper map[string]Service
	Cache  map[string]*Msg

	*prometheus.HistogramVec
	sync.RWMutex // lock for cache
}

func (this *Services) Call(req *Msg, env ...interface{}) (rsp *Msg, ok bool) { // const for no copy
	// func (this Services) Call(req *Msg, env ...interface{}) (rsp *Msg, ok bool) { // can't set
	timer := prometheus.NewTimer(this.HistogramVec.WithLabelValues(req.Id))
	defer func() {
		timer.ObserveDuration()
	}()
	ok = true
	key := req.Key4req()
	timeout, err4cache := ParseDuration(req.Cached)
	if err4cache == nil && timeout > time.Millisecond {
		ms4now := time.Now().UnixMilli()
		rsp = func() (msg *Msg) {
			this.RLock()
			defer this.RUnlock()
			var found bool
			if msg, found = this.Cache[key]; found {
				if msg.UnixMilli4exe+timeout.Milliseconds() > ms4now {
					return msg
				} // else cached timeout
			} // else no cached
			return nil
		}()
		if rsp != nil {
			return
		}
		req.UnixMilli4exe = ms4now
	}

	rsp = req
	ok = true
	if svi, found := this.Mapper[req.Name]; found {
		var err error
		defer func() {
			if r := recover(); r != nil {
				if rsp == nil {
					rsp = req
				}
				rsp.Code = stack.Caller(3).String()
				rsp.Info = fmt.Sprintf("Panic service call(%s,%v)@%p err:%v,%v:%s:%v",
					Marshal4json(req), env, this, err, r, debug.Stack(), stack.Trace().TrimRuntime())
				ok = false
			}
		}()
		rsp, err = svi(req, env...)
		if err != nil {
			if rsp == nil {
				rsp = req
			}
			rsp.Code = stack.Caller(1).String()
			rsp.Info = fmt.Sprintf("Error service call(%s,%v)@%p err:%v", Marshal4json(req), env, this, err)
			ok = false
		}
	} else {
		rsp.Code = stack.Caller(1).String()
		rsp.Info = fmt.Sprintf("Not found service call(%v,%v)@%p", Marshal4json(req), env, this)
		ok = false
	}

	if err4cache == nil {
		//func() {
		this.Lock()
		defer this.Unlock()
		this.Cache[key] = req
		//}()
	}
	return
}

type Job struct {
	*Msg

	Archived      *bool  `json:"arch,omitempty"`
	Cron          string `json:"cron,omitempty"`
	*CronList     `json:"-"`
	UnixMilli4trg int64 `json:"um4trg,omitempty"`
	Ids4prev      Set   `json:"prev,omitempty"` // check the parent job done
	//Ids4next      Set   `json:"next,omitempty"` // send msg to the listener, support by etcd watch
	// TODO need marshal/unmarshal and implements in server
	Retry         int    `json:"retry,omitempty"`
	Retry4max     int    `json:"retry4max,omitempty"`
	UnixMilli4ttl int64  `json:"um4ttl,omitempty"`
	Id4sync       string `json:"id4sync,omitempty"`

	EditorId      string          `json:"edr,omitempty"`
	UnixMilli4mod int64           `json:"um4mod,omitempty"`
	Note          string          `json:"note,omitempty"`
	Ext           json.RawMessage `json:"ext,omitempty"`
}

func (this *Job) String() string {
	// nil panic by *CronList
	// %!v(PANIC=String method: runtime error: invalid memory address or nil pointer dereference)
	return String(this.DsvMarshal(0, 0))
}

// DsvHeader strings.Join(hdr, ",")
func (this *Job) DsvHeader() (hdr []string) {
	return []string{"id", "name", "req", "rsp", "data", "code", "info", "um4exe", "cached", // 0-8
		"arch", "cron", "um4trg", "prev", "edr", "um4mod", "note", "ext"}
}
func (this *Job) DsvUnmarshal(delimiter, quote byte, data []byte) (err error) {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	cols := Dsv2cols(data, delimiter, quote)

	id_ := String(cols[0])
	name_ := String(cols[1])
	req_ := cols[2]
	rsp_ := cols[3]
	data_ := String(cols[4])
	code_ := String(cols[5])
	info_ := String(cols[6])
	var um4exe_ int64
	um4exe_, err = strconv.ParseInt(String(cols[7]), 10, 64)
	if err != nil {
		return fmt.Errorf("Job.DsvUnmarshal um4exe strconv.ParseInt(String(cols[7]), 10, 64) err:%v", err)
	}
	cached_ := String(cols[8])

	var arch_ *bool
	tmp4arch_ := strings.ToLower(strings.TrimSpace(String(cols[9])))
	if tmp4arch_ != "" {
		if tmp4arch_ == "1" || tmp4arch_ == "t" || tmp4arch_ == "true" {
			arch_ = &TRUE
		} else {
			arch_ = &FALSE
		}
	}
	cron_ := String(cols[10])
	var um4trg_ int64
	um4trg_, err = strconv.ParseInt(String(cols[11]), 10, 64)
	if err != nil {
		return fmt.Errorf("Job.DsvUnmarshal um4trg strconv.ParseInt(String(cols[11]), 10, 64) err:%v", err)
	}
	var ids4prev_ Set
	Dsv2arr(cols[12], delimiter, quote, func(bytes []byte) {
		if ids4prev_ == nil {
			ids4prev_ = Set{}
		}
		ids4prev_[String(bytes)] = Sizeof0
	})
	editorId_ := String(cols[13])
	var um4mod_ int64
	um4mod_, err = strconv.ParseInt(String(cols[14]), 10, 64)
	if err != nil {
		return fmt.Errorf("Job.DsvUnmarshal um4mod strconv.ParseInt(String(cols[14]), 10, 64) err:%v", err)
	}
	note_ := String(cols[15])
	ext_ := cols[16]
	this.Msg = &Msg{
		id_,
		name_,
		req_,
		rsp_,
		data_,
		code_,
		info_,
		um4exe_,
		cached_,
	}
	this.Archived = arch_
	this.Cron = cron_
	this.UnixMilli4trg = um4trg_
	this.Ids4prev = ids4prev_
	this.EditorId = editorId_
	this.UnixMilli4mod = um4mod_
	this.Note = note_
	this.Ext = ext_
	return
}
func (this *Job) DsvMarshal(delimiter, quote byte) (data []byte) {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	data = make([]byte, 0, 512)
	if this.Msg != nil {
		data = append(data, String2dsv(this.Id, delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(this.Name, delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(String(this.Req), delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(String(this.Rsp), delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(this.Data, delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(this.Code, delimiter, quote)...)
		data = append(append(data, delimiter), String2dsv(this.Info, delimiter, quote)...)
		data = append(append(data, delimiter), strconv.FormatInt(this.UnixMilli4exe, 10)...)
		data = append(append(data, delimiter), String2dsv(this.Cached, delimiter, quote)...)
	} else {
		//data = append(data, ",,,,,,,0,"...)
		//data = bytes.Repeat([]byte{delimiter}, 7)
		data = append(data, delimiter, delimiter, delimiter, delimiter, delimiter, delimiter, delimiter, '0', delimiter)
	}
	data = append(data, delimiter)
	if this.Archived != nil {
		data = append(data, fmt.Sprintf("%t", *this.Archived)...)
	}
	data = append(append(data, delimiter), String2dsv(this.Cron, delimiter, quote)...)
	data = append(append(data, delimiter), strconv.FormatInt(this.UnixMilli4trg, 10)...)
	data = append(append(data, delimiter), String2dsv(func() string {
		keys := make([][]byte, 0, len(this.Ids4prev))
		for key := range this.Ids4prev {
			keys = append(keys, Slice(key))
		}
		return String(Arr2dsv(keys, delimiter, quote))
	}(), delimiter, quote)...)
	data = append(append(data, delimiter), String2dsv(this.EditorId, delimiter, quote)...)
	data = append(append(data, delimiter), strconv.FormatInt(this.UnixMilli4mod, 10)...)
	data = append(append(data, delimiter), String2dsv(this.Note, delimiter, quote)...)
	data = append(append(data, delimiter), String2dsv(String(this.Ext), delimiter, quote)...)
	return data
}

func (this *Job) Triggering(now time.Time) (trg bool, um4next int64, err error) { // const
	if this.Archived != nil {
		if *this.Archived { // need archived job
			um4next = -1
		}
		//else { // ignore error job and wait debug
		//	um4next = now.UnixMilli() + 1
		//}
		return
	}
	if this.Cron == "" {
		if this.UnixMilli4trg > now.UnixMilli() {
			um4next = this.UnixMilli4trg
		} else {
			trg = true
			um4next = -1
		}
	} else {
		um4next = this.UnixMilli4trg
		if this.UnixMilli4trg >= 0 {
			if this.CronList == nil {
				var cron *CronList
				cron, err = NewCronList(this.Cron)
				if err != nil {
					err = fmt.Errorf("triggering %v", err)
					return
				}
				this.CronList = cron
			}
			time4next, offsets := this.CronList.Next(now)
			if len(offsets) > 0 {
				um4next2 := time4next.UnixMilli()
				if this.UnixMilli4trg > now.UnixMilli() {
					// trg = false
					if this.UnixMilli4trg > um4next2 {
						um4next = um4next2
					}
					//else {
					//	um4next = this.UnixMilli4trg
					//}
				} else {
					trg = true
					um4next = um4next2
				}
			} else { // end of cycle
				if this.UnixMilli4trg > now.UnixMilli() {
					//trg = false
					//um4next = this.UnixMilli4trg
				} else {
					trg = true
					um4next = -1
				}
			}
		}
	}
	return
}
func (this *Job) Triggered(now time.Time) (trg bool) { // const
	if this.Cron == "" {
		if this.UnixMilli4exe > 0 {
			return this.UnixMilli4exe < now.UnixMilli()
		} else {
			return false
		}
	} else {
		var trg2 bool
		var um4next int64
		var err error
		if this.UnixMilli4exe <= 0 { // never trigger
			trg2, um4next, err = this.Triggering(now)
			if err != nil {
				log.Warn("triggered never trigger", "now", now, "job", Marshal4json(this), "err", err)
				return false
			}
			return !trg2 && um4next < 0 // not need trigger
		} else { // this.UnixMilli4exe > 0 // it had been triggered
			tm4exe := time.UnixMilli(this.UnixMilli4exe)
			tm4exe = tm4exe.In(now.Location())
			trg2, um4next, err = this.Triggering(tm4exe)
			if err != nil {
				log.Warn("triggered have triggered", "tm4exe", tm4exe, "job", Marshal4json(this), "err", err)
				return false
			}
			//if trg2 {
			//
			//} else { // !trg2
			//	if um4next < 0 { // not need trigger
			//		return true
			//	}
			//}
			if !trg2 && um4next < 0 {
				return true // not need trigger
			} else { // if um4next > 0 {
				um4now := now.UnixMilli()
				return this.UnixMilli4exe < um4now && um4now < um4next
			}
			//else { // um4next == 0
			//	return false // trigger now
			//}
		}
	}
}

func (this *Job) Sort(rhs *Job, asc bool) (cmp int) {
	if this == rhs {
		return 0
	}
	if this == nil {
		cmp = -1
	} else if rhs == nil {
		cmp = 1
	} else {
		cmp = int(this.UnixMilli4trg - rhs.UnixMilli4trg)
		if cmp == 0 {
			if this.Msg == rhs.Msg {
				return 0
			} else if this.Msg == nil {
				cmp = -1
			} else if rhs.Msg == nil {
				cmp = 1
			} else {
				cmp = strings.Compare(this.Msg.Id, rhs.Msg.Id)
			}
		}
	}
	if !asc {
		cmp = 0 - cmp
	}
	return
}
