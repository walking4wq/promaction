package util

import (
	"testing"
	"time"
)

func TestJob_Archived(t *testing.T) {
	const (
		archivedIsNil   = `{}`
		archivedIsTrue  = `{"arch":true}`
		archivedIsFalse = `{"arch":false}`
	)
	job := &Job{}
	str4json := Marshal4json(job)
	t.Logf("Marshal4json(%v) return:%s", job, str4json)
	if archivedIsNil != str4json {
		t.Errorf("Marshal4json(%v) return:%s not except:%s", job, str4json, archivedIsNil)
	}
	job.Archived = &FALSE
	str4json = Marshal4json(job)
	t.Logf("Marshal4json(%v) return:%s", job, str4json)
	if archivedIsFalse != str4json {
		t.Errorf("Marshal4json(%v) return:%s not except:%s", job, str4json, archivedIsFalse)
	}
	job.Archived = &TRUE
	str4json = Marshal4json(job)
	t.Logf("Marshal4json(%v) return:%s", job, str4json)
	if archivedIsTrue != str4json {
		t.Errorf("Marshal4json(%v) return:%s not except:%s", job, str4json, archivedIsTrue)
	}
	job.Archived = nil
	str4json = Marshal4json(job)
	t.Logf("Marshal4json(%v) return:%s", job, str4json)
	if archivedIsNil != str4json {
		t.Errorf("Marshal4json(%v) return:%s not except:%s", job, str4json, archivedIsNil)
	}
}

func TestMsg_UnixMilli(t *testing.T) {
	now := time.Now()
	utc4now := now.UTC()
	um4now := now.UnixMilli()
	um4utc := utc4now.UnixMilli()
	t.Logf("now[%v]=%d, utc[%v]=%d", now, um4now, utc4now, um4utc)
	if um4now != um4utc {
		t.Errorf("not equals now[%v]=%d, utc[%v]=%d", now, um4now, utc4now, um4utc)
	}
	tm4utc := time.UnixMilli(um4utc).UTC()
	tm4now := tm4utc.In(now.Location())
	um4utc = tm4utc.UnixMilli()
	um4now = tm4now.UnixMilli()
	t.Logf("tm4utc[%v]=%d, tm4now[%v]=%d", tm4utc, um4now, tm4now, um4utc)
	if um4now != um4utc {
		t.Errorf("not equals tm4utc[%v]=%d, tm4now[%v]=%d", tm4utc, um4now, tm4now, um4utc)
	}
}
