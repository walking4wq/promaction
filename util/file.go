package util

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"container/list"
	"fmt"
	log "github.com/inconshreveable/log15"
	"io"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"time"
)

// PathExist https://studygolang.com/articles/5435
func PathExist(path string) (bool, os.FileInfo, error) {
	fi, err := os.Stat(path)
	if err == nil {
		return true, fi, nil
	} else if os.IsNotExist(err) {
		return false, nil, nil
	}
	return false, nil, err
}

type WalkFn4readLine func(line int, data4line []byte, last bool) (brk bool, err error)

func ReadLine(filename string, read io.Reader, walkFn WalkFn4readLine) (err error) {
	if read == nil {
		// ioutil.ReadFile(filename)
		file, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer func() {
			_ = file.Close() // ignore error to avoid Unhandled error warning
		}()
		read = file
	}
	rl := bufio.NewReader(read)
	brk := false
	var prev []byte
	for i := -1; ; i++ {
		buff := bytes.Buffer{}
		line, isPrefix, err := rl.ReadLine() // bufio.ScanLines
		for isPrefix && err == nil {
			buff.Write(line)
			line, isPrefix, err = rl.ReadLine()
		}
		if buff.Len() > 0 {
			buff.Write(line)
			line = buff.Bytes()
		}
		// line, err := readLine(r)
		if err == io.EOF {
			if i >= 0 {
				_, err = walkFn(i, prev, true)
			} else {
				err = nil
			}
			brk = true
			goto label4err
		} else if err != nil {
			return fmt.Errorf("read line for file[%s] err:%v", filename, err)
		}
		if i < 0 {
			prev = []byte(String(line)) // bytes.Copy(line)
		} else {
			brk, err = walkFn(i, prev, false)
			prev = []byte(String(line)) // bytes.Copy(line)
		}
	label4err:
		if err != nil {
			return fmt.Errorf("read line[%d] walk for file[%s] err:%v", i, filename, err)
		}
		if brk {
			return nil
		}
	}
}

// Readdirnames ref readDirNames by go/src/path/filepath/path.go in go1.15.15.linux-amd64/
func Readdirnames(dirname string, buff int, asc *bool) ([]string, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()
	names, err := f.Readdirnames(buff)
	if err != nil {
		return nil, err
	}
	if asc != nil {
		if *asc {
			sort.Strings(names)
		} else {
			r := sort.StringSlice(names[0:])
			sort.Sort(sort.Reverse(r))
			names = r
		}
	}
	return names, nil
}
func Readdir(dirname string, buff int, asc *bool) ([]os.FileInfo, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()
	dirs, err := f.Readdir(buff)
	if err != nil {
		return nil, err
	}
	if asc != nil {
		if *asc {
			sort.Slice(dirs, func(i, j int) bool {
				return dirs[i].Name() < dirs[j].Name()
			})
		} else {
			sort.Slice(dirs, func(i, j int) bool {
				return dirs[i].Name() > dirs[j].Name()
			})
		}
	}
	return dirs, nil
}

type WalkFn4readLimit func(idx int, buff []byte, more bool) (brk bool, cnt uint64, err error)

func ReadLimit(filename string, read io.Reader, limit, count int, walkFn WalkFn4readLimit) (err error) {
	if read == nil {
		// ioutil.ReadFile(filename)
		file, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer func() {
			_ = file.Close() // ignore error to avoid Unhandled error warning
		}()
		read = file
	}
	len4buf := limit * count
	buf := make([]byte, len4buf+limit)
	buf2 := make([]byte, len4buf)
	var rd, rd2, idx int
	var eof, brk bool
	var offset, cnt uint64
	rdr := bufio.NewReaderSize(read, len4buf)
	rd, err = rdr.Read(buf) // read.Read(buf) // io.ReadFull(rdr, buf)
	if err == io.EOF {
		eof = true
	} else if err != nil {
		return err
	}
	for offset < uint64(rd) {
		if !eof && uint64(rd)-offset <= uint64(limit) {
			rd = copy(buf, buf[offset:rd])
			rd2, err = rdr.Read(buf2) // read.Read(buf2) // io.ReadFull(rdr, buf2)
			if err == io.EOF {
				eof = true
			} else if err != nil {
				return err
			}
			if rd2 > 0 {
				rd2 = copy(buf[rd:], buf2[0:rd2])
				rd += rd2
			}
			offset = 0
		}
		brk, cnt, err = walkFn(idx, buf[offset:rd], !eof)
		if err != nil {
			return fmt.Errorf("read limit(%d,byte[%d]%d:%d,%t) err:%v", idx, len4buf+limit, offset, rd, !eof, err)
		}
		if brk {
			break
		}
		idx++
		offset += cnt
	}
	return nil
}

func Import4fileReader() []string { return []string{"bufio", "fmt", "io", "os"} }

const Go4fileReader = `
type WalkFn4readLine func(line int, data4line []byte, last bool) (brk bool, err error)

func ReadLine(filename string, read io.Reader, walkFn WalkFn4readLine) (err error) {
	if read == nil {
		// ioutil.ReadFile(filename)
		file, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer func() {
			_ = file.Close() // ignore error to avoid Unhandled error warning
		}()
		read = file
	}
	rl := bufio.NewReader(read)
	brk := false
	var prev []byte
	for i := -1; ; i++ {
		line, isPrefix, err := rl.ReadLine() // bufio.ScanLines
		for isPrefix && err == nil {
			var bs []byte
			bs, isPrefix, err = rl.ReadLine()
			line = append(line, bs...)
		}
		// line, err := readLine(r)
		if err == io.EOF {
			if i >= 0 {
				_, err = walkFn(i, prev, true)
			} else {
				err = nil
			}
			brk = true
			goto label4err
		} else if err != nil {
			return fmt.Errorf("read line for file[%s] err:%v", filename, err)
		}
		if i < 0 {
			prev = []byte(String(line)) // bytes.Copy(line)
		} else {
			brk, err = walkFn(i, prev, false)
			prev = []byte(String(line)) // bytes.Copy(line)
		}
	label4err:
		if err != nil {
			return fmt.Errorf("read line[%d] walk for file[%s] err:%v", i, filename, err)
		}
		if brk {
			return nil
		}
	}
}

type WalkFn4readLimit func(idx int, buff []byte, more bool) (brk bool, cnt uint64, err error)

func ReadLimit(filename string, read io.Reader, limit, count int, walkFn WalkFn4readLimit) (err error) {
	if read == nil {
		// ioutil.ReadFile(filename)
		file, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer func() {
			_ = file.Close() // ignore error to avoid Unhandled error warning
		}()
		read = file
	}
	len4buf := limit * count
	buf := make([]byte, len4buf+limit)
	buf2 := make([]byte, len4buf)
	var rd, rd2, idx int
	var eof, brk bool
	var offset, cnt uint64
	rdr := bufio.NewReaderSize(read, len4buf)
	rd, err = rdr.Read(buf) // read.Read(buf) // io.ReadFull(rdr, buf)
	if err == io.EOF {
		eof = true
	} else if err != nil {
		return err
	}
	for offset < uint64(rd) {
		if !eof && uint64(rd)-offset <= uint64(limit) {
			rd = copy(buf, buf[offset:rd])
			rd2, err = rdr.Read(buf2) // read.Read(buf2) // io.ReadFull(rdr, buf2)
			if err == io.EOF {
				eof = true
			} else if err != nil {
				return err
			}
			if rd2 > 0 {
				rd2 = copy(buf[rd:], buf2[0:rd2])
				rd += rd2
			}
			offset = 0
		}
		brk, cnt, err = walkFn(idx, buf[offset:rd], !eof)
		if err != nil {
			return fmt.Errorf("read limit(%d,byte[%d]%d:%d,%t) err:%v", idx, len4buf+limit, offset, rd, !eof, err)
		}
		if brk {
			break
		}
		idx++
		offset += cnt
	}
	return nil
}
`

type Naming4pipe func() (dir, key, seq string)
type Writer4pipeMeta struct {
	Wnd4time time.Duration
	Max4line int
	Naming   Naming4pipe
}
type Writer4pipe struct {
	*Writer4pipeMeta
	Println bool
	Gzip    bool

	dir, filename string
	file          *os.File
	file4gz       *gzip.Writer

	timestamp time.Time
	count     int
}

func (this *Writer4pipe) Close() (err error) {
	if this.file == nil {
		return
	}
	filename := this.filename
	if this.Gzip {
		filename += ".gz"
		err = this.file4gz.Close()
		if err != nil {
			err = fmt.Errorf("gzip.NewWriter[%s,%s]%v Close err:%v", this.dir, filename, this, err)
			return
		}
	}
	err = this.file.Close()
	if err != nil {
		err = fmt.Errorf("os.Create[%s,%s]%v Close err:%v", this.dir, filename, this, err)
		return
	}
	file4doing := fmt.Sprintf("%s.doing", filename)
	pathFile4doing := filepath.Join(this.dir, file4doing)
	pathFile := filepath.Join(this.dir, filename)
	err = os.Rename(pathFile4doing, pathFile)
	if err != nil {
		err = fmt.Errorf("os.Rename[%s]%v for commit file err:%v", pathFile4doing, this, err)
		return
	}
	this.file = nil
	return
}
func (this *Writer4pipe) Write(data []byte) (wrt int, err error) {
	//if this.count > 0 && (this.count >= this.Max4line || time.Now().Sub(this.timestamp) >= this.Wnd4time || data == nil) {
	if this.count >= this.Max4line || time.Now().Sub(this.timestamp) >= this.Wnd4time || data == nil {
		err = this.Close()
		if err != nil {
			return
		}
	}
	if data == nil {
		return
	}
	if this.file == nil {
		this.dir, this.filename = "", ""
		if this.Naming != nil {
			var key, seq string
			this.dir, key, seq = this.Naming()
			if seq == "" {
				this.filename = key
			} else {
				this.filename = fmt.Sprintf("%s-%s", key, seq)
			}
		}
		if "" == this.filename {
			this.filename = "-" + time.Now().Format(FMT4TIMESTAMP)
		}
		filename := this.filename
		if this.Gzip {
			filename += ".gz"
		}
		err = os.MkdirAll(this.dir, 0755) // 0666)
		if err != nil {
			err = fmt.Errorf("os.MkdirAll(%s, 0755) err:%v", this.dir, err)
			return
		}
		var file *os.File
		pathFile4doing := filepath.Join(this.dir, fmt.Sprintf("%s.doing", filename))
		file, err = os.Create(pathFile4doing)
		if err != nil {
			err = fmt.Errorf("os.Create(%s) file err:%v", pathFile4doing, err)
			return
		}
		this.file = file
		if this.Gzip {
			this.file4gz = gzip.NewWriter(file)
		}
		this.timestamp = time.Now()
		this.count = 0
	}
	if this.Gzip {
		if this.Println && this.count > 0 {
			wrt, err = fmt.Fprintln(this.file4gz)
		}
		wrt, err = this.file4gz.Write(data)
	} else {
		if this.Println && this.count > 0 {
			wrt, err = fmt.Fprintln(this.file)
		}
		wrt, err = this.file.Write(data)
	}
	if err != nil {
		return
	}
	this.count++
	return
}
func (this *Writer4pipe) Run() (rcv chan<- []byte, wg *sync.WaitGroup) {
	chn := make(chan []byte, CAP4CHANNEL)
	rcv = chn
	wg = &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		tick := time.Tick(this.Wnd4time)
		var wrt int
		var err error
		for {
			select {
			case data, ok := <-chn:
				wrt, err = this.Write(data)
				if err != nil {
					log.Error("pipe Write err", fmt.Sprintf("%q", data), this, "wrt", wrt, "err", err)
				}
				if !ok {
					//this.Close()
					return
				}
			case <-tick:
				wrt, err = this.Write(nil)
				if err != nil {
					log.Error("pipe Write close err", "writer", this, "wrt", wrt, "err", err)
				}
			}
		}
	}()
	return
}

// LRUWriter4pipe
// Least recently used
// https://studygolang.com/articles/23183
type LRUWriter4pipe struct {
	Max   int
	list  *list.List
	cache map[string]*list.Element
}
type node4LRUWriter4pipe struct {
	*Writer4pipe
	rcv chan<- []byte
	wg  *sync.WaitGroup
	key string
}

func NewLRUWriter4pipe(max int) (writer *LRUWriter4pipe) {
	writer = &LRUWriter4pipe{
		max, list.New(), make(map[string]*list.Element, max),
	}
	return
}
func (this *LRUWriter4pipe) Close() {
	this.aging(0)
}
func (this *LRUWriter4pipe) Write(meta *Writer4pipeMeta, data []byte) {
	key := ""
	if meta.Naming != nil {
		_, key, _ = meta.Naming()
	}
	ele, found := this.cache[key]
	if !found {
		this.aging(this.Max)
		if meta.Wnd4time < time.Second {
			meta.Wnd4time = TimeWnd4pipeWriter
		}
		if meta.Max4line < 3 {
			meta.Max4line = LineMax4pipeWriter
		}
		writer := &Writer4pipe{Writer4pipeMeta: meta, Println: true, Gzip: true}
		rcv, wg := writer.Run()
		ele = this.list.PushFront(&node4LRUWriter4pipe{writer, rcv, wg, key})
		this.cache[key] = ele
	}
	ele.Value.(*node4LRUWriter4pipe).rcv <- data
	this.list.MoveToFront(ele)
}

func (this *LRUWriter4pipe) aging(max int) {
	var ele *list.Element
	var node *node4LRUWriter4pipe
	for this.list.Len() > max {
		ele = this.list.Back()
		if ele == nil {
			break
		}
		this.list.Remove(ele)
		node = ele.Value.(*node4LRUWriter4pipe)
		delete(this.cache, node.key)
		close(node.rcv)
		node.wg.Wait()
	}
}

type LRUWriter4pipeData struct {
	Meta *Writer4pipeMeta
	Data []byte
}

func (this *LRUWriter4pipe) Run() (rcv chan<- *LRUWriter4pipeData, wg *sync.WaitGroup) {
	chn := make(chan *LRUWriter4pipeData, CAP4CHANNEL)
	rcv = chn
	wg = &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer this.Close()
		for {
			select {
			case data, ok := <-chn:
				if !ok {
					return
				}
				this.Write(data.Meta, data.Data)
			}
		}
	}()
	return
}
