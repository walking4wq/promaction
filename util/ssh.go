package util

import (
	"context"
	"fmt"
	log "github.com/inconshreveable/log15"
	"golang.org/x/crypto/ssh"
	"io"
	"sync"
	"time"
)

func NewShell(conn *ssh.Client, h4pty, w4pty int, modes ssh.TerminalModes) (sess *ssh.Session, waiter *sync.WaitGroup,
	stdin io.WriteCloser, stdout, stderr io.Reader, err error) {
	sess, err = conn.NewSession() // OpenChannel("session", nil)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			if err4cls := sess.Close(); err4cls != nil {
				log.Warn("session.Close err", "err", err4cls)
			}
		}
	}()
	if h4pty < 40 {
		h4pty = 40
	}
	if w4pty < 80 {
		w4pty = 80
	}
	if len(modes) < 1 {
		// Set up terminal modes
		modes = ssh.TerminalModes{
			ssh.ECHO:          0,     // disable echoing
			ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
			ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
		}
	}
	// Request pseudo terminal
	if err = sess.RequestPty("xterm", h4pty, w4pty, modes); err != nil {
		err = fmt.Errorf("request for pseudo terminal err:%v", err)
		return
	}
	stdin, err = sess.StdinPipe()
	if err != nil {
		err = fmt.Errorf("get stdin pipe for write cmd to shell err:%v", err)
		return
	}
	stdout, err = sess.StdoutPipe()
	if err != nil {
		err = fmt.Errorf("get stdout pipe for read cmd result err:%v", err)
		return
	}
	stderr, err = sess.StderrPipe()
	if err != nil {
		err = fmt.Errorf("get stderr pipe for read cmd result err:%v", err)
		return
	}
	// Start remote shell
	if err = sess.Shell(); err != nil {
		err = fmt.Errorf("start shell err:%v", err)
		return
	}
	waiter = &sync.WaitGroup{}
	waiter.Add(1)
	go func() {
		defer waiter.Done()
		log.Info("start session wait", "sess", sess)
		if err = sess.Wait(); err != nil {
			log.Warn("failed to wait shell", "sess", sess, "err", err)
		}
		log.Info("end session wait", "sess", sess)
	}()
	return
}

// https://github.com/melbahja/goph/blob/0ae87d82dd767ce23ab243a5e8a969cd98c2c417/cmd.go#L103
//// Executes the given callback within session. Sends SIGINT when the context is canceled.
//func (c *Cmd) runWithContext(callback func() ([]byte, error)) ([]byte, error) {
//	outputChan := make(chan ctxCmdOutput)
//	go func() {
//		output, err := callback()
//		outputChan <- ctxCmdOutput{
//			output: output,
//			err:    err,
//		}
//	}()
//
//	select {
//	case <-c.Context.Done():
//		_ = c.Session.Signal(ssh.SIGINT)
//
//		return nil, c.Context.Err()
//	case result := <-outputChan:
//		return result.output, result.err
//	}
//}

func BCall(fn func() (out interface{}), cxl func(), timeout time.Duration) (out interface{}, err error) {
	chan4out := make(chan interface{})
	go func() {
		chan4out <- fn()
	}()
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	select {
	case <-ctx.Done():
		cxl()
		return nil, ctx.Err() // context.DeadlineExceeded
	case out = <-chan4out:
		return
	}
}

type Out4cmd struct {
	Out []byte `json:"out,omitempty"`
	Err error  `json:"err,omitempty"`
}

func Exec4ssh(conn *ssh.Client, cmd string, timeout time.Duration) (out []byte, err error) {
	var sess *ssh.Session
	sess, err = conn.NewSession() // OpenChannel("session", nil)
	if err != nil {
		return
	}
	defer func() {
		if err := sess.Close(); err != nil {
			log.Debug("session.Close for cmd", cmd, err)
		}
	}()
	rst, err := BCall(func() (out interface{}) {
		rst, err := sess.CombinedOutput(cmd)
		return &Out4cmd{rst, err}
	}, func() {
		if err := sess.Signal(ssh.SIGINT); err != nil {
			log.Warn("session.Signal(ssh.SIGINT) for cmd", cmd, err)
		}
	}, timeout)
	if err != nil {
		return
	}
	out4cmd := rst.(*Out4cmd)
	return out4cmd.Out, out4cmd.Err
}
