package util

import "fmt"

func DefaultDelimiter4dsv(delimiter byte) byte {
	if delimiter == 0 {
		return Delimiter4dsv
	} else {
		return delimiter
	}
}
func DefaultQuote4dsv(quote byte) byte {
	if quote == 0 {
		return Quote4dsv
	} else {
		return quote
	}
}

func DefinedDsvDefaultOption(defined Set) string {
	if Generated(defined, "DefinedDsvDefaultOption") {
		return ""
	}
	return `
func DefaultDelimiter4dsv(delimiter byte) byte {
	if delimiter == 0 {
		return '` + fmt.Sprintf("%c", Delimiter4dsv) + `'
	} else {
		return delimiter
	}
}
func DefaultQuote4dsv(quote byte) byte {
	if quote == 0 {
		return '` + String(String2dsv(fmt.Sprintf("%c", Quote4dsv), Delimiter4dsv, Quote4dsv)) + `'
	} else {
		return quote
	}
}
`
	// fmt.Sprintf("%c", Quote4dsv) + `'
}

// String2dsv https://www.postgresql.org/docs/9.6/sql-copy.html
// Dsv Delimiter-Separated Values
func String2dsv(str string, delimiter, quote byte) []byte {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	var special4quote2dsv = map[byte]byte{
		delimiter: delimiter, quote: quote,
		'\b': 'b', // Backspace (ASCII 8)
		'\f': 'f', // Form feed (ASCII 12)
		'\n': 'n', // Newline (ASCII 10)
		'\r': 'r', // Carriage return (ASCII 13)
		'\t': 't', // Tab (ASCII 9)
		'\v': 'v', // Vertical tab (ASCII 11)
	}
	buff := make([]byte, 0, int(float64(len(Slice(str)))*1.2))
	for _, byt := range Slice(str) {
		if char, found := special4quote2dsv[byt]; found {
			buff = append(buff, quote, char)
		} else {
			buff = append(buff, byt)
		}
	}
	return buff
}

func Dsv2cols(dsv []byte, delimiter, quote byte) [][]byte {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	var dsv2special4quote = map[byte]byte{
		delimiter: delimiter, quote: quote,
		'b': '\b', // Backspace (ASCII 8)
		'f': '\f', // Form feed (ASCII 12)
		'n': '\n', // Newline (ASCII 10)
		'r': '\r', // Carriage return (ASCII 13)
		't': '\t', // Tab (ASCII 9)
		'v': '\v', // Vertical tab (ASCII 11)
	}
	var cols [][]byte
	fld := make([]byte, 0) // var fld []byte // is nil
	quoted := false
	for _, byt := range dsv {
		switch byt {
		case delimiter:
			if quoted {
				if char, found := dsv2special4quote[byt]; found {
					fld = append(fld, char)
				} else {
					fld = append(fld, quote, byt)
				}
				quoted = false
			} else {
				cols = append(cols, fld)
				fld = []byte{}
			}
		case quote:
			if quoted {
				fld = append(fld, quote)
				quoted = false
			} else {
				quoted = true
			}
		default:
			if quoted {
				if char, found := dsv2special4quote[byt]; found {
					fld = append(fld, char)
				} else {
					fld = append(fld, quote, byt)
				}
				quoted = false
			} else {
				fld = append(fld, byt)
			}
		}
	}
	if quoted {
		fld = append(fld, quote)
	}
	cols = append(cols, fld)
	return cols
}

func DefinedDsv2string(defined Set) (import4jit, go4jit string) {
	if Generated(defined, "DefinedDsv2string") {
		return "", ""
	}
	import4jit, go4jit = DefinedBytes2string(defined)
	go4jit += DefinedDsvDefaultOption(defined)
	return import4jit, go4jit + `
// String2dsv https://www.postgresql.org/docs/9.6/sql-copy.html
// Dsv Delimiter-Separated Values
func String2dsv(str string, delimiter, quote byte) []byte {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	var special4quote2dsv = map[byte]byte{
		delimiter: delimiter, quote: quote,
		'\b': 'b', // Backspace (ASCII 8)
		'\f': 'f', // Form feed (ASCII 12)
		'\n': 'n', // Newline (ASCII 10)
		'\r': 'r', // Carriage return (ASCII 13)
		'\t': 't', // Tab (ASCII 9)
		'\v': 'v', // Vertical tab (ASCII 11)
	}
	buff := make([]byte, 0, int(float64(len(Slice(str)))*1.2))
	for _, byt := range Slice(str) {
		if char, found := special4quote2dsv[byt]; found {
			buff = append(buff, quote, char)
		} else {
			buff = append(buff, byt)
		}
	}
	return buff
}

func Dsv2cols(dsv []byte, delimiter, quote byte) [][]byte {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	var dsv2special4quote = map[byte]byte{
		delimiter: delimiter, quote: quote,
		'b': '\b', // Backspace (ASCII 8)
		'f': '\f', // Form feed (ASCII 12)
		'n': '\n', // Newline (ASCII 10)
		'r': '\r', // Carriage return (ASCII 13)
		't': '\t', // Tab (ASCII 9)
		'v': '\v', // Vertical tab (ASCII 11)
	}
	var cols [][]byte
	var fld []byte
	quoted := false
	for _, byt := range dsv {
		switch byt {
		case delimiter:
			if quoted {
				if char, found := dsv2special4quote[byt]; found {
					fld = append(fld, char)
				} else {
					fld = append(fld, quote, byt)
				}
				quoted = false
			} else {
				cols = append(cols, fld)
				fld = []byte{}
			}
		case quote:
			if quoted {
				fld = append(fld, quote)
				quoted = false
			} else {
				quoted = true
			}
		default:
			if quoted {
				if char, found := dsv2special4quote[byt]; found {
					fld = append(fld, char)
				} else {
					fld = append(fld, quote, byt)
				}
				quoted = false
			} else {
				fld = append(fld, byt)
			}
		}
	}
	if quoted {
		fld = append(fld, quote)
	}
	cols = append(cols, fld)
	return cols
}
`
}

func Strings2dsv(strs []string, delimiter, quote byte) []byte {
	delimiter = DefaultDelimiter4dsv(delimiter)
	quote = DefaultQuote4dsv(quote)
	buff := make([]byte, 0, len(strs)*16)
	for i, str := range strs {
		if i > 0 {
			buff = append(buff, delimiter)
		}
		buff = append(buff, String2dsv(str, delimiter, quote)...)
	}
	return buff
}
