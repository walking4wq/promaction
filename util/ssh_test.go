package util

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

func TestSsh_base(t *testing.T) {
	// https://www.cnblogs.com/zhzhlong/p/12552410.html
	// 建立SSH客户端连接
	client, err := ssh.Dial("tcp", "10.45.121.8:22", &ssh.ClientConfig{
		User:            "walking",
		Auth:            []ssh.AuthMethod{ssh.Password("1")},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		t.Fatalf("SSH dial error: %s", err.Error())
	}
	defer func() {
		if err := client.Close(); err != nil {
			t.Errorf("client.Close err:%v", err)
		}
	}()
	// 建立新会话
	session, err := client.NewSession()
	if err != nil {
		t.Fatalf("new session error: %s", err.Error())
	}
	defer func() {
		if err := session.Close(); err != nil {
			t.Logf("session.Close err:%v", err)
		}
	}()

	cmd := "ls -al"
	//result, err := session.Output(cmd)

	var buff bytes.Buffer
	session.Stdout = &buff
	err = session.Run(cmd)
	if err != nil {
		t.Fatalf("Failed to run command:%s, err:%v", cmd, err)
	}
	t.Logf(">%s\n%s\n*** end ***", cmd, buff.String())

	cmd = "pwd"
	err = session.Run(cmd)
	if err == nil || err.Error() != "ssh: session already started" {
		t.Fatalf("Failed to run command:%s, err:%v", cmd, err)
	}
	t.Logf(">%s\n%s\n*** end ***", cmd, buff.String())
}

func clt4ssh(t *testing.T) (clt *ssh.Client) {
	addr4ssh := "10.45.121.8:22"
	user := "walking"
	pwd := "1"

	// go/pkg/mod/golang.org/x/crypto@v0.0.0-20220722155217-630584e8d5aa/ssh/example_test.go:143
	host := "hostname"
	file, err := os.Open(filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts_10.45.121.8"))
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			t.Errorf("ssh known hosts file.Close err:%v", err)
		}
	}()
	scanner := bufio.NewScanner(file)
	var hostKey ssh.PublicKey
	var comment string
	var options []string
	var rest []byte
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), " ")
		if len(fields) != 3 {
			continue
		}
		t.Logf(strings.Join(fields, ","))
		//if strings.Contains(fields[0], host) {
		//	var err error
		hostKey, comment, options, rest, err = ssh.ParseAuthorizedKey(scanner.Bytes())
		if err != nil {
			t.Fatalf("error parsing %q: %v", fields[2], err)
		}
		t.Logf("public key:%v:%s, comment:%s, options[%d]:%s, rest:%s",
			hostKey, String(hostKey.Marshal()), comment, len(options), strings.Join(options, "*"), String(rest))

		//if strings.Contains(comment, host) {
		//	break
		//}
		//hostKey = nil
		// the last key is right key
		//}
		break
	}
	if hostKey == nil {
		t.Fatalf("no hostkey for %s", host)
	}

	// Create client config
	config := &ssh.ClientConfig{
		User: user, Auth: []ssh.AuthMethod{ssh.Password(pwd)},
		HostKeyCallback: ssh.FixedHostKey(hostKey), // ssh.InsecureIgnoreHostKey()
	}
	// Connect to ssh server
	clt, err = ssh.Dial("tcp", addr4ssh, config)
	if err != nil {
		t.Fatal("unable to connect: ", err)
	}
	return
}

// exec ref by https://github.com/helloyi/go-sshclient/blob/6ede888765baecc7808e9c75749a76442d978915/sshclient.go#L241
func exec(t *testing.T, clt *ssh.Client, cmd string) (out []byte) {
	// Create a session
	sess, err := clt.NewSession()
	if err != nil {
		t.Fatal("unable to create session: ", err)
	}
	defer func() {
		if err := sess.Close(); err != nil {
			t.Logf("session.Close err:%v", err)
		}
	}()
	out, err = sess.Output(cmd)
	if err != nil {
		t.Fatalf("Failed to exec command:%s, err:%v", cmd, err)
	}
	return
}

func TestSsh_session(t *testing.T) {
	// https://blog.csdn.net/weixin_43912821/article/details/122021959
	// /go/pkg/mod/golang.org/x/crypto@v0.0.0-20220722155217-630584e8d5aa/ssh/example_test.go:285
	conn := clt4ssh(t)
	defer func() {
		if err := conn.Close(); err != nil {
			t.Errorf("client.Close err:%v", err)
		}
	}()
	session, wg, wrt4cmd, rdr4cmd, stderr, err := NewShell(conn, 40, 80, nil)
	if err != nil {
		t.Fatal("NewShell err:", err)
	}
	defer func() {
		if err := session.Close(); err != nil {
			t.Logf("session.Close err:%v", err)
		}
	}()
	_ = stderr
	//// Create a session
	//session, err := conn.NewSession()
	//if err != nil {
	//	t.Fatal("unable to create session: ", err)
	//}
	//defer func() {
	//	if err := session.Close(); err != nil {
	//		t.Logf("session.Close err:%v", err)
	//	}
	//}()
	//// Set up terminal modes
	//modes := ssh.TerminalModes{
	//	ssh.ECHO:          0,     // disable echoing
	//	ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
	//	ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	//}
	//// Request pseudo terminal
	//if err := session.RequestPty("xterm", 40, 80, modes); err != nil {
	//	t.Fatal("request for pseudo terminal failed: ", err)
	//}
	//
	//wrt4cmd, err := session.StdinPipe()
	//if err != nil {
	//	t.Fatalf("get stdin pipe for write cmd to shell error%v\n", err)
	//}
	//rdr4cmd, err := session.StdoutPipe()
	//if err != nil {
	//	t.Fatalf("get stdout pipe for read cmd execute result error%v\n", err)
	//}
	//
	//// Start remote shell
	//if err = session.Shell(); err != nil {
	//	t.Fatal("failed to start shell: ", err)
	//}
	//
	//var wg sync.WaitGroup
	//wg.Add(1)
	//go func() {
	//	defer wg.Done()
	//	t.Log("start session wait")
	//	if err = session.Wait(); err != nil {
	//		t.Log("failed to wait shell: ", err)
	//	}
	//	t.Log("end session wait")
	//}()

	cmds := []string{
		"ls -al\n",
		"ls\n",
		"pwd\n",
		"ssh root@192.168.166.218\n",
		"1\n",
		"ls -al\n",
		"exit\n",
		"exit\n",
	}
	var rst []byte
	var wtn int
	var buff []byte
	// terminator := '$'
	code4biz := false
	for i, cmd := range cmds {
		//rst, err = session.Output(cmd) // ssh: session already started/set
		//goto EOL
		wtn, err = wrt4cmd.Write(Slice(cmd))
		if err != nil {
			t.Errorf("%3d >%s, err:%d,%v", i, cmd, wtn, err)
		}
		t.Logf("%3d >%s, wtn:%d", i, cmd, wtn)
		//if strings.HasPrefix(cmd, "exit\n") {
		//	break
		//}
		//rst, err = ioutil.ReadAll(rdr4cmd)
		buff = make([]byte, 1024*8)
		rst = rst[:0]
		//if c.User == "root" {
		//	terminator = '#'
		//}
		for first := true; true; first = false {
			if first {
				time.Sleep(time.Millisecond * 200)
			}
			if code4biz {
				wtn, err = rdr4cmd.Read(buff)
				t.Logf("shell read[%d]:%d[%q] err:%v\nrst:%d[%q]", wtn, len(buff), buff[:wtn], err, len(rst), rst)
				if err != nil {
					if err != io.EOF {
						t.Errorf("read out buffer err:%v", err)
					} else {
						err = nil
					}
					break
				}
				if wtn > 0 {
					rst = append(rst, buff[:wtn]...)
					// idx4end := bytes.LastIndexByte(buff, byte(terminator))
					// if idx4end > 0 {
					length := len(rst)
					if bytes.Compare(rst[length-2:length], []byte{'$', ' '}) == 0 ||
						bytes.Compare(rst[length-2:length], []byte{'#', ' '}) == 0 ||
						strings.HasSuffix(String(rst), "'s password: ") {
						break
						//_ = 0
					}
				}
				//break // observe whether to read the unread output of the previous command // it will read
			} else {
				var out4call interface{}
				out4call, err = BCall(func() (out interface{}) {
					wtn, err = rdr4cmd.Read(buff)
					t.Logf("shell read[%d]:%d[%q] err:%v\nrst:%d[%q]", wtn, len(buff), buff[:wtn], err, len(rst), rst)
					out = &Out4cmd{buff[:wtn], err}
					return
				}, func() {
					err := session.Signal(ssh.SIGINT)
					t.Logf("session.Signal(ssh.SIGINT) for [%s] err:%v", cmd, err)
					// need restart shell session after ctrl+c
					err = session.Close()
					if err != nil {
						t.Logf("session.Close for restart session err:%v", err)
					}
					session, wg, wrt4cmd, rdr4cmd, stderr, err = NewShell(conn, 40, 80, nil)
					if err != nil {
						t.Fatal("NewShell err:", err)
					}
				}, time.Second*3)
				if err == nil {
					out4cmd := out4call.(*Out4cmd)
					err = out4cmd.Err
					if err != nil {
						if err != io.EOF {
							t.Errorf("read out buffer err:%v", err)
						} else {
							err = nil
						}
						break
					} else {
						rst = append(rst, out4cmd.Out...)
						// biz hard coding for check session
						length := len(rst)
						if len(rst) > 2 && (bytes.Compare(rst[length-2:length], []byte{'$', ' '}) == 0 ||
							bytes.Compare(rst[length-2:length], []byte{'#', ' '}) == 0 ||
							strings.HasSuffix(String(rst), "'s password: ")) {
							break
							//_ = 0
						}
					}
				} else if err == context.DeadlineExceeded {
					err = nil
					// rdr4cmd, err = session.StdoutPipe() // err[ssh: StdoutPipe after process started]
					break
				} else {
					t.Errorf("read out buffer unexcept err:%v", err)
					break
				}
			}
		}
		// go/pkg/mod/golang.org/x/crypto@v0.0.0-20220722155217-630584e8d5aa/ssh/session_test.go:121
		//var buff bytes.Buffer
		//var written int64
		//if written, err = io.Copy(&buff, rdr4cmd); err != nil {
		//	t.Errorf("Copy of stdout failed:%d,%v", written, err)
		//}
		//rst = buff.Bytes()
		//EOL:
		if err != nil {
			t.Errorf("%d >%s, return:[%d]%q:%s, err:%v", i, cmd, len(rst), rst, String(rst), err)
		}
		fmt.Printf("%d >%s\n[%d]%q\n{%s}\n*** end ***\n", i, cmd, len(rst), rst, String(rst))
	}
	wg.Wait()
	t.Log("Game Over!")
}

func TestSsh_cmds(t *testing.T) {
	conn := clt4ssh(t)
	defer func() {
		if err := conn.Close(); err != nil {
			t.Errorf("client.Close err:%v", err)
		}
	}()
	cmds := []string{
		"ls -al\n",
		"ls\n",
		"pwd\n",
		"date\n",
		"sleep 5\n",
		"date\n",
		//"ssh root@192.168.166.218\n",
		//"1\n",
		//"ls -al\n",
		//"exit\n",
		"exit\n",
	}
	var out []byte
	var err error
	for i, cmd := range cmds {
		if i < 3 {
			out = exec(t, conn, cmd)
		} else {
			out, err = Exec4ssh(conn, cmd, time.Second*3)
		}
		if err == nil {
			t.Logf("%d >%s\n[%d]%q\n{%s}\n*** end ***\n", i, cmd, len(out), out, String(out))
		} else if err == context.DeadlineExceeded {
			t.Logf("%d >%s\ntimeout:%v\n[%d]%q\n{%s}\n*** end ***\n", i, cmd, err, len(out), out, String(out))
		} else {
			t.Errorf("%d >%s\nerror:%v\n[%d]%q\n{%s}\n*** end ***\n", i, cmd, err, len(out), out, String(out))
		}
	}
}
