package util

import (
	"errors"
	"fmt"
	"sync/atomic"
	"time"
)

// https://zhuanlan.zhihu.com/p/39205152
// https://github.com/zhenorzz/snowflake/blob/master/snowflake.go
// Package snowflake provides a very simple Twitter snowflake generator and parser.
// +-------------------------------------------------------+
// | 42 Bit Timestamp | 10 Bit WorkID | 12 Bit Sequence ID |
// +-------------------------------------------------------+
// 20210223033045 00 999
// 2021022303304500999
//  9223372036854775807
// 12345678901234567890
//  2102240954009999999

// hilo seq

const (
	MaxDigit4seq = 7
)

type HiloTime struct {
	uuid uint64
	// const
	node                      uint32
	loc                       *time.Location
	maxSeq, maxNode, timeDecs uint32
}

func NewHiloTime(nodeId uint32, loc *time.Location, maxSeqDecs, maxNodeDecs uint32) (*HiloTime, error) {
	//  9223372036854775807
	// 12345678901234567890
	//  2102240954009999999
	//              7654321
	if maxSeqDecs+maxNodeDecs > MaxDigit4seq { // precision
		return nil, errors.New(fmt.Sprintf("too long dec digit %d+%d>%d", maxSeqDecs, maxNodeDecs, MaxDigit4seq))
	}
	var maxSeq uint32 = 1
	var maxNode uint32 = 1
	max := maxSeqDecs
	if max < maxNodeDecs {
		max = maxNodeDecs
	}
	for i := uint32(0); i < max; i++ {
		if i < maxSeqDecs {
			maxSeq *= 10
		}
		if i < maxNodeDecs {
			maxNode *= 10
		}
	}
	if nodeId > maxNode {
		return nil, errors.New(fmt.Sprintf("invalid node[0,%d) id:%d", maxNode, nodeId))
	}
	timeDecs := maxSeq * maxNode
	uuid := pack4ht(time2sec(loc), 0, timeDecs, maxNode, nodeId)
	ht := &HiloTime{uuid, nodeId, loc, maxSeq, maxNode, timeDecs}
	return ht, nil
}

func time2sec(loc *time.Location) uint64 {
	tm := time.Now()
	if loc != nil {
		tm = tm.In(loc)
	}
	year := tm.Year() % 100
	// @formatter:off
	return uint64(year*10000000000 +
		int(tm.Month())*100000000 +
		tm.Day()*1000000 +
		tm.Hour()*10000 +
		tm.Minute()*100 +
		tm.Second())
	// @formatter:on
}
func (this *HiloTime) Generate() uint64 {
	for {
		old := atomic.LoadUint64(&this.uuid)
		tm, seq, _ := unpack4ht(old, this.timeDecs, this.maxNode)
		//fmt.Printf("unpack4ht:%d,tm:%d,seq:%d\n", old, tm, seq)
		seq++
		if seq < this.maxSeq {
			newVal := pack4ht(tm, seq, this.timeDecs, this.maxNode, this.node)
			//fmt.Printf("uuid:%d->%d,tm:%d,seq:%d\n", old, new, tm, seq)
			if atomic.CompareAndSwapUint64(&this.uuid, old, newVal) {
				return newVal
			}
		} else {
			new4tm := time2sec(this.loc)
			if new4tm > tm {
				newVal := pack4ht(new4tm, 0, this.timeDecs, this.maxNode, this.node)
				//fmt.Printf("uuid:%d->%d,tm:%d->%d\n", old, new, tm, new4tm)
				if atomic.CompareAndSwapUint64(&this.uuid, old, newVal) {
					return newVal
				}
			}
			time.Sleep(100 * time.Millisecond)
		}
	}
}
func pack4ht(tm uint64, seq uint32, timeDecs, maxNode, nodeId uint32) uint64 {
	return tm*uint64(timeDecs) + uint64(seq*maxNode+nodeId)
}
func unpack4ht(uuid uint64, timeDecs, maxNode uint32) (tm uint64, seq, nodeId uint32) {
	tm = uuid / uint64(timeDecs)
	others := uint32(uuid % uint64(timeDecs))
	seq = others / maxNode
	nodeId = others % maxNode
	return
}

/*
type HiloTime struct {
	tm  uint64
	seq uint32
	// const
	node                      uint32
	loc                       *time.Location
	maxSeq, maxNode, timeDecs uint32
}

// New returns a new snowflake node that can be used to generate snowflake
func NewHiloTime(nodeId uint32, loc *time.Location, maxSeqDecs, maxNodeDecs uint32) (*HiloTime, error) {
	//  9223372036854775807
	// 12345678901234567890
	//  2102240954009999999
	//              7654321
	if maxSeqDecs+maxNodeDecs > MaxDigit4seq { // precision
		return nil, errors.New(fmt.Sprintf("too long dec digit %d+%d>%d", maxSeqDecs, maxNodeDecs, MaxDigit4seq))
	}
	var maxSeq uint32 = 1
	var maxNode uint32 = 1
	max := maxSeqDecs
	if max < maxNodeDecs {
		max = maxNodeDecs
	}
	for i := uint32(0); i < max; i++ {
		if i < maxSeqDecs {
			maxSeq *= 10
		}
		if i < maxNodeDecs {
			maxNode *= 10
		}
	}
	if nodeId > maxNode {
		return nil, errors.New(fmt.Sprintf("invalid node[0,%d) id:%d", maxNode, nodeId))
	}
	ht := &HiloTime{time2sec(loc), 0, nodeId, loc, maxSeq, maxNode, maxSeq * maxNode}
	//go func(hltm *HiloTime) {
	//	tick := time.Tick(time.Millisecond * 100)
	//	for {
	//		select {
	//		case <-tick:
	//			old := hltm.tm // atomic.LoadUint64(&ht.tm)
	//			new := time2sec(hltm.loc)
	//			// if atomic.CompareAndSwapUint64(&ht.tm, old, new) {
	//			if new > old {
	//				hltm.tm = new // atomic.StoreUint64(&ht.tm, new)
	//				atomic.StoreUint32(&hltm.seq, 0)
	//			}
	//		}
	//	}
	//}(ht)
	return ht, nil
}
func (this *HiloTime) Generate() uint64 {
	//var seq uint32
	//for {
	//	//seq = atomic.LoadUint32(&this.seq)
	//	//if seq+1 < MaxSeqDecs && atomic.CompareAndSwapUint32(&this.seq, seq, seq+1) {
	//	//	break
	//	//}
	//	seq = atomic.AddUint32(&this.seq, 1)
	//	if seq < this.maxSeq {
	//		break
	//	}
	//	time.Sleep(100 * time.Microsecond)
	//}
	//new := atomic.LoadUint64(&this.tm)
	var new uint64
	var seq uint32
	for {
		new = atomic.LoadUint64(&this.tm)
		seq = atomic.AddUint32(&this.seq, 1)
		if seq < this.maxSeq {
			break
		}
		old := new
		new = time2sec(this.loc)
		if new > old && atomic.CompareAndSwapUint64(&this.tm, old, new) {
			atomic.StoreUint32(&this.seq, 0)
			seq = 0
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	return this.pack(new, seq)
}
func time2sec(loc *time.Location) uint64 {
	tm := time.Now()
	if loc != nil {
		tm = tm.In(loc)
	}
	year := tm.Year() % 100
	// @formatter:off
    return uint64(year*10000000000 +
       	int(tm.Month())*100000000 +
               tm.Day()*1000000 +
              tm.Hour()*10000 +
            tm.Minute()*100 +
            tm.Second())
	// @formatter:on
}
func (this HiloTime) pack(tm uint64, seq uint32) uint64 {
	return tm*uint64(this.timeDecs) + uint64(seq*this.maxNode+this.node)
}
*/

// Package snowflake provides a very simple Twitter snowflake generator and parser.
// +-------------------------------------------------------+
// | 42 Bit Timestamp | 10 Bit WorkID | 12 Bit Sequence ID |
// +-------------------------------------------------------+
const (
	epoch           int64 = 1526285084373
	numWorkerBits         = 10
	numSequenceBits       = 12
	MaxWorkId             = -1 ^ (-1 << numWorkerBits)
	MaxSequence           = -1 ^ (-1 << numSequenceBits)

	Seq0xFF = 0xFFF
	Wkr0xFF = 0x3FF000
	Tim0xFF = 0xFFFFFFFFFFC00000 // uint64(-1) ^ (uint64(-1)<<numWorkerBits + numSequenceBits)
)

type SnowFlake struct {
	uuid uint64
	// const
	lastTimestamp uint64
	sequence      uint32
	workerId      uint32
	// lock          sync.Mutex
}

func pack4sf(tm uint64, workerId, seq uint32) uint64 {
	uuid := (tm << (numWorkerBits + numSequenceBits)) | (uint64(workerId) << numSequenceBits) | (uint64(seq))
	return uuid
}
func unpack4sf(uuid uint64) (tm uint64, workerId, seq uint32) {
	tm = uuid >> (numWorkerBits + numSequenceBits)
	seq = uint32(uuid &^ (Tim0xFF | Wkr0xFF))
	workerId = uint32(uuid &^ Tim0xFF &^ Seq0xFF >> numSequenceBits)
	return
}

// NewSnowFlake New returns a new snowflake node that can be used to generate snowflake
func NewSnowFlake(workerId uint32) (*SnowFlake, error) {
	if workerId < 0 || workerId > MaxWorkId {
		return nil, errors.New("invalid worker Id")
	}
	tm := timestamp()
	uuid := pack4sf(tm, workerId, 0)
	sf := &SnowFlake{uuid: uuid, lastTimestamp: tm, workerId: workerId}
	//go func(sf *SnowFlake) {
	//	tick := time.Tick(100 * time.Microsecond)
	//	for {
	//		select {
	//		case <-tick:
	//			old := sf.lastTimestamp // atomic.LoadUint64(&sf.lastTimestamp)
	//			new := timestamp()
	//			if new > old { // if atomic.CompareAndSwapUint64(&sf.lastTimestamp, old, new) {
	//				sf.lastTimestamp = new // atomic.StoreUint64(&sf.lastTimestamp, new)
	//				atomic.StoreUint32(&sf.sequence, 0)
	//			}
	//		}
	//	}
	//}(sf)
	return sf, nil
}

// Generate Next creates and returns a unique snowflake ID
func (sf *SnowFlake) Generate() uint64 { // } (uint64, error) {
	//var new uint64
	//var seq uint32
	//for {
	//	old := atomic.LoadUint64(&sf.lastTimestamp)
	//	seq = atomic.AddUint32(&sf.sequence, 1)
	//	new = atomic.LoadUint64(&sf.lastTimestamp)
	//	if seq < MaxSequence && old == new { // seq&MaxSequence != 0 {
	//		break
	//	}
	//	old = new
	//	new = timestamp()
	//	if new > old && atomic.CompareAndSwapUint64(&sf.lastTimestamp, old, new) {
	//		atomic.StoreUint32(&sf.sequence, 0)
	//		seq = 0
	//		break
	//	}
	//	//time.Sleep(10 * time.Microsecond)
	//}
	//uuid := (new << (numWorkerBits + numSequenceBits)) | (uint64(sf.workerId) << numSequenceBits) | (uint64(seq))
	//return uuid
	for {
		old := atomic.LoadUint64(&sf.uuid)
		tm, _, seq := unpack4sf(old)
		//fmt.Printf("unpack4ht:%d,tm:%d,seq:%d\n", old, tm, seq)
		seq++
		if seq < MaxSequence {
			newVal := pack4sf(tm, sf.workerId, seq)
			//fmt.Printf("uuid:%d->%d,tm:%d,seq:%d\n", old, new, tm, seq)
			if atomic.CompareAndSwapUint64(&sf.uuid, old, newVal) {
				return newVal
			}
		} else {
			new4tm := timestamp()
			if new4tm > tm {
				newVal := pack4sf(new4tm, sf.workerId, 0)
				//fmt.Printf("uuid:%d->%d,tm:%d->%d\n", old, new, tm, new4tm)
				if atomic.CompareAndSwapUint64(&sf.uuid, old, newVal) {
					return newVal
				}
			}
			time.Sleep(100 * time.Microsecond)
		}
	}
}

//func (sf *SnowFlake) pack() uint64 {
//	uuid := (sf.lastTimestamp << (numWorkerBits + numSequenceBits)) | (uint64(sf.workerId) << numSequenceBits) | (uint64(sf.sequence))
//	return uuid
//}
//// waitNextMilli if that microsecond is full
//// wait for the next microsecond
//func (sf *SnowFlake) waitNextMilli(ts uint64) uint64 {
//	for ts == sf.lastTimestamp {
//		time.Sleep(100 * time.Microsecond)
//		ts = timestamp()
//	}
//	return ts
//}

// timestamp
func timestamp() uint64 {
	return uint64(time.Now().UnixNano()/int64(1000000) - epoch)
}
