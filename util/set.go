package util

type Void struct{} // sizeof is 0
var Sizeof0 Void

type Set map[string]Void // interface{}

func DefinedSet(defined Set) (go4jit string) {
	if Generated(defined, "DefinedSet") {
		return ""
	}
	return `
type Void struct{} // sizeof is 0
var Sizeof0 Void

type Set map[string]Void // interface{}
`
}
