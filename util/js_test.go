package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/robertkrimen/otto"
	"testing"
)

func TestJsMapping(t *testing.T) {
	hdl := "mapping"
	src := fmt.Sprintf(`
function %s(json4in, type4rtn) {
  console.log("in=", json4in, String.fromCharCode.apply(String, json4in));
  obj = JSON.parse(String.fromCharCode.apply(String, json4in));
  console.log("in obj.one=", obj.one);
  if (type4rtn == "bool") {
    return typeof obj.one === 'undefined'
  }
  var obj4rst = {
    rst: 9
  };
  console.log(typeof(obj4rst), obj4rst.rst);
  obj4rst.rst = obj.one;
  if (type4rtn == "json") {
    return JSON.stringify(obj4rst);
  } else {
    return obj4rst
  }
}
`, hdl)
	in := json.RawMessage(`
{"one":1,"two":2}
`)
	type4rtns := []string{"bool", "json", "object"}
	var vm *otto.Otto
	rdr4src := bytes.NewBufferString(src)
	var out otto.Value
	var err error
	for _, typ := range type4rtns {
		//obj4in := map[string]interface{}{"one": 1, "two": 2}
		//in = json.RawMessage(Marshal4json(obj4in))
		//in = json.RawMessage(`[]`)
		vm, out, err = JsMapping(nil, rdr4src, hdl, in, typ)
		if err != nil {
			t.Errorf("JsMapping[%s] call(%s,%s,%s) err:%v", src, hdl, String(in), typ, err)
		} else {
			t.Logf("JsMapping[%s] call(%s,%s,%s) return:%v,%v,%v", src, hdl, String(in), typ, vm, out, err)
		}
		switch typ {
		case "object":
			if out.IsObject() {
				t.Logf("object keys:%v", out.Object().Keys())
				for i, key := range out.Object().Keys() {
					val, err := out.Object().Get(key)
					t.Logf("object %d [%s]:[%v] err:%v", i, key, val, err)
				}
			} else {
				t.Errorf("not return object")
			}
		case "json":
			if out.IsString() {
				str, err := out.ToString()
				t.Logf("string:%s,err:%v", str, err)
			} else {
				t.Errorf("not return json string")
			}
		case "bool":
			if out.IsBoolean() {
				bol, err := out.ToBoolean()
				t.Logf("bool:%t,err:%v", bol, err)
			} else {
				t.Errorf("not return bool")
			}
		default:
			t.Errorf("unsupported typ:%s!", typ)
		}
	}
}
