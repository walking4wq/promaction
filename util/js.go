package util

import (
	"encoding/json"
	log "github.com/inconshreveable/log15"
	"github.com/robertkrimen/otto"
	"io"
)

// js engine, search www.github.com by[go javascript]: https://github.com/robertkrimen/otto

func JsMapping(vm *otto.Otto, src io.Reader, hdl string, in json.RawMessage,
	args ...interface{}) (vm4out *otto.Otto, out otto.Value, err error) {
	if vm == nil {
		vm4out, out, err = otto.Run(src)
	} else {
		vm4out = vm
		out, err = vm.Run(src)
	}
	log.Debug("js mapping run", "vm", vm, "src", src, "out", out, "err", err)
	if err != nil {
		return
	}
	if hdl != "" {
		out, err = vm4out.Call(hdl, nil, in, args)
	}
	return
}
