package util

// Paging
//  if end >= cnt then is the last page
//  if idx == 0 then is the first page
func Paging(cnt, siz4page, idx4page int) (begin, end, idx int) {
	if cnt <= 0 {
		return 0, 0, 0
	}
	if siz4page <= 0 {
		return 0, cnt, 0
	}
	if idx4page < 0 {
		idx4page = 0
	}
	// pages := int(math.Ceil(float64(cnt) / float64(siz4page)))
	pages := cnt / siz4page
	if cnt%siz4page > 0 {
		pages++
	}
	idx = idx4page
	if idx4page >= pages {
		idx = pages - 1
	}
	begin = siz4page * idx
	end = begin + siz4page
	if end > cnt {
		end = cnt
	}
	return
}

type Page struct {
	Siz int `json:"siz,omitempty"`
	Idx int `json:"idx,omitempty"`
	// paging rsp
	Cnt int `json:"cnt,omitempty"`
}
type Sort struct {
	By     string `json:"by"`
	Asc    bool   `json:"asc,omitempty"`
	Offset int    `json:"offset,omitempty"`
}
