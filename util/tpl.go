package util

import "fmt"

func Generated(set4once Set, identifier string) bool {
	if set4once == nil {
		return false
	}
	_, found := set4once[identifier]
	if !found {
		set4once[identifier] = Sizeof0
	}
	return found
}

func Import(defined Set, pkg string) (import4jit string) {
	identifier := fmt.Sprintf(`import "%s"`, pkg)
	if !Generated(defined, identifier) {
		import4jit = identifier
	}
	return
}
