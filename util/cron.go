package util

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"strings"
	"time"
)

const (
	Separator = "|"
	// MaxLoop4Next = 24
)

type offset4cron struct {
	cron.Schedule
	//*cronexpr.Expression
	offsets []int
}
type CronList map[string]*offset4cron

func (this CronList) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("%d=[", len(this)))
	for k, v := range this {
		sb.WriteString(k)
		sb.WriteByte('=')
		sb.WriteString(fmt.Sprintf("%v", v.offsets))
		sb.WriteByte(',')
	}
	str := sb.String()
	if str[len(str)-1] == ',' {
		return str[0:len(str)-1] + "]"
	} else {
		return str + "]"
	}
}

func NewCronList(exprs string) (cronlist *CronList, err error) {
	expr4keys := strings.Split(exprs, Separator)
	cl := make(CronList, len(expr4keys))
	//var expr4cron *cronexpr.Expression
	var sched cron.Schedule
	specParser := cron.NewParser(cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow) // "0 0 15 */3 *"
	for offset, expr := range expr4keys {
		co, found := cl[expr]
		if !found {
			sched, err = specParser.Parse(expr)
			if err != nil {
				return nil, fmt.Errorf("cron.Parse[%v].Parse(%s)[%d/%d] err:%v",
					specParser, expr, offset, len(expr4keys), err)
			}
			//expr4cron, err = cronexpr.Parse(expr)
			//if err != nil {
			//	return nil, fmt.Errorf("cronexpr.Parse(%s)[%d/%d] err:%v",
			//		expr, offset, len(expr4keys), err)
			//}
			offsets := []int{offset}
			//co = &offset4cron{sched, expr4cron, offsets}
			co = &offset4cron{sched, offsets}
			cl[expr] = co
		} else {
			co.offsets = append(co.offsets, offset)
		}
	}
	cronlist = &cl
	return
}

//func Next(expr *cronexpr.Expression, time4curr time.Time) time.Time {
//	time4next := expr.Next(time4curr)
//	if time4next.IsZero() {
//		return time4next
//	}
//	if !time4next.After(time4curr) {
//		loc := time4curr.Location()
//		time4next = expr.Next(time4curr.UTC())
//		time4next = time4next.In(loc)
//	}
//	return time4next
//	//// skip the [2021-03-14 00:00:00 -0800 PST,2021-03-14 03:00:00 -0800 PST]
//	//for i := 1; !time4next.After(time4curr) && i < MaxLoop4Next; i++ {
//	//	log.Warn(fmt.Sprintf("expr.Next(%v)=[%v] no slide to the future %d times", time4curr, time4next, i))
//	//	time4next = expr.Next(time4curr.Add(time.Duration(i) * time.Hour))
//	//}
//	loc := time4curr.Location()
//	time4nexts := expr.NextN(time4curr.UTC(), 2)
//	if !time4next.After(time4curr) {
//		log.Warn(fmt.Sprintf("expr.Next(%v)=[%v] no slide to the future and try to using UTC", time4curr, time4next))
//		time4next = expr.Next(time4curr.Add(1 * time.Minute))
//		for i := 1; !time4next.After(time4curr) && i < MaxLoop4Next; i++ {
//			time4next = expr.Next(time4curr.Add(time.Duration(i+1) * 5 * time.Minute))
//		}
//		time4next1 := expr.Next(time4nexts[0])
//		if time4next.Equal(time4next1) {
//			time4next = time4nexts[0].In(loc)
//		}
//	} else if time4nexts[0].Before(time4next) {
//		if time4next.Equal(time4nexts[1]) {
//			log.Warn(fmt.Sprintf("expr.Next(%v)=[%v] slide too long by UTC[%v]", time4curr, time4next, time4nexts[0]))
//			time4next = time4nexts[0].In(loc)
//		}
//	}
//	return time4next
//}

func (this CronList) Next(fromTime time.Time) (time4next time.Time, offsets []int) {
	time4next = time.Time{}
	var time4tmp time.Time
	for expr, offset4cron := range this {
		_ = expr
		//time4tmp = Next(offset4cron.Expression, fromTime) // offset4cron.Next(fromTime)
		time4tmp = offset4cron.Schedule.Next(fromTime)
		if time4tmp.IsZero() {
			continue
		}
		if time4next.IsZero() || time4tmp.Before(time4next) {
			time4next = time4tmp
			offsets = offset4cron.offsets
		} else if time4tmp.Equal(time4next) {
			offsets = append(offsets, offset4cron.offsets...)
		}
	}
	return
}
