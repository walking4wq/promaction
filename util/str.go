package util

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
	"runtime"
	"strings"
	"time"
	"unsafe"
)

const (
	FMT4DAY          = "20060102"
	FMT4TIMESTAMP    = "20060102150405"
	FMT4TIMESTAMPZ   = FMT4TIMESTAMP + "Z0700" // 20210617170000-0700 20211217032600-0800 20211217032600Z 20211217032600+0800
	FMT4TIMESTAMP_MS = "20060102150405.999"
	FMT4TIME         = "2006-01-02T15:04:05"
	FMT4TIME_WEEK    = "2006-01-02T15:04:05_Mon"
	FMT4TIMEZONE     = time.RFC3339 // Asia/Colombo UTC+05:30
	FMT4PGCOPYTMZ    = "20060102T150405Z0700"
	FMT4MYSQLLOADTMZ = time.RFC3339 // "2006-01-02T15:04:05Z0700"
)

// Time2nbr format: "20060102150405.9999"
func Time2nbr(tm time.Time) uint64 {
	/*
		00010000000000 0000
		20210223033040.7194
		2021
		2
		23
		3
		30
		40
		719406884
	*/
	// @formatter:off
	return uint64(tm.Year()*100000000000000 +
		int(tm.Month())*1000000000000 +
		tm.Day()*10000000000 +
		tm.Hour()*100000000 +
		tm.Minute()*1000000 +
		tm.Second()*10000 +
		int(tm.Nanosecond()/100000))
	// @formatter:on
}
func Time2sec(tm time.Time) uint64 {
	// @formatter:off
	return uint64(tm.Year()*10000000000 +
		int(tm.Month())*100000000 +
		tm.Day()*1000000 +
		tm.Hour()*10000 +
		tm.Minute()*100 +
		tm.Second())
	// @formatter:on
}

func Truncate2day(tm time.Time) time.Time {
	// https://blog.csdn.net/qqqqll3/article/details/103956922
	//timeStr := tm.Format("2006-01-02")
	//t, err := time.ParseInLocation("2006-01-02", timeStr, tm.Location())
	//return t
	// return time.Date(tm.Year(), tm.Month(), tm.Day(), 0, 0, 0, 0, tm.Location())
	y, m, d := tm.Date()
	return time.Date(y, m, d, 0, 0, 0, 0, tm.Location())
}
func Truncate2hhmi(tm time.Time) time.Time {
	return time.Date(1970, 1, 1, tm.Hour(), tm.Minute(), 0, 0, tm.Location())
}

// var line string
// bytes := *(*[]byte)(unsafe.Pointer(&line)) // 遇到bytes的len大于0，cap为0的情况，操作bytes的时候，会panic
// var d []byte
// line = *(*string)(unsafe.Pointer(&d))

// const ( // copy from src/github.com/emicklei/go-restful/constants.go
// 	MIME_XML = "application/xml"
// )
// bytes := *(*[]byte)(unsafe.Pointer(&MIME_XML)) // cannot take the address of MIME_XML
// fmt.Printf("%v:%s\n", bytes, MIME_XML)

// String https://www.cnblogs.com/zhouj-happy/p/12486878.html
// github.com/savsgio/gotils@v0.0.0-20210907153846-c06938798b52/strconv/strconv.go
// B2S converts byte slice to a string without memory allocation.
// See https://groups.google.com/forum/#!msg/Golang-Nuts/ENgbUzYvCuU/90yGx7GUAgAJ .
// same as: github.com/go-redis/redis/v9@v9.0.0-beta.2/internal/util/unsafe.go:11
func String(b []byte) (s string) {
	//pbytes := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	//pstring := (*reflect.StringHeader)(unsafe.Pointer(&s))
	//pstring.Data = pbytes.Data
	//pstring.Len = pbytes.Len
	//return
	return *(*string)(unsafe.Pointer(&b))
}

// Slice
// S2B converts string to a byte slice without memory allocation.
//
// Note it may break if string and/or slice header will change
// in the future go versions.
func Slice(s string) (b []byte) {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	bh.Data = sh.Data
	bh.Cap = sh.Len
	bh.Len = sh.Len

	runtime.KeepAlive(&s)

	return b
}

//type Walk4dsv2arr func([]byte)

func Dsv2arr(dsv []byte, delimiter, quote byte, walker func([]byte)) {
	if len(dsv) < 1 {
		return
	}
	cols := Dsv2cols(dsv, delimiter, quote)
	lth := len(cols) - 1
	var ept bool
	ept4all := true
	for i := 0; i <= lth; i++ {
		ept = len(cols[i]) == 0
		if !ept {
			ept4all = false
		}
		if i == lth && ept4all {
			break
		}
		walker(cols[i])
	}
	return
}

func Arr2dsv(arr [][]byte, delimiter, quote byte) (dsv []byte) {
	lth := len(arr) - 1
	if lth < 0 {
		return
	}
	var ept bool
	ept4all := true
	for i := 0; i <= lth; i++ {
		ept = len(arr[i]) == 0
		if !ept {
			ept4all = false
		}
		dsv = append(dsv, String2dsv(String(arr[i]), delimiter, quote)...)
		if i == lth && !ept4all {
			break
		}
		dsv = append(dsv, DefaultDelimiter4dsv(delimiter))
	}
	return
}

func DefinedBytes2string(defined Set) (import4jit, go4jit string) {
	if Generated(defined, "DefinedBytes2string") {
		return "", ""
	}
	sb := &strings.Builder{}
	sb.Grow(64)
	WriteString(sb, "", false, Import(defined, "reflect"))
	WriteString(sb, "", false, Import(defined, "runtime"))
	WriteString(sb, "", false, Import(defined, "unsafe"))
	return sb.String(), `
func String(b []byte) (s string) {
	return *(*string)(unsafe.Pointer(&b))
}
func Slice(s string) (b []byte) {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	bh.Data = sh.Data
	bh.Cap = sh.Len
	bh.Len = sh.Len

	runtime.KeepAlive(&s)

	return b
}
func Dsv2arr(dsv []byte, delimiter, quote byte, walker func([]byte)) {
	if len(dsv) < 1 {
		return
	}
	cols := Dsv2cols(dsv, delimiter, quote)
	lth := len(cols)
	lth_ := lth - 1
	for i := 0; i < lth; i++ {
		if i == lth_ && len(cols[i]) == 0 {
			break
		}
		walker(cols[i])
	}
	return
}
func Arr2dsv(arr [][]byte, delimiter, quote byte) (dsv []byte) {
	lth := len(arr) - 1
	if lth < 0 {
		return
	}
	var ept bool
	ept4all := true
	for i := 0; i <= lth; i++ {
		ept = len(arr[i]) == 0
		if !ept {
			ept4all = false
		}
		dsv = append(dsv, String2dsv(String(arr[i]), delimiter, quote)...)
		if i == lth && !ept4all {
			break
		}
		dsv = append(dsv, DefaultDelimiter4dsv(delimiter))
	}
	return
}
`
}

func Log(msg string, ctx ...interface{}) string {
	var buff bytes.Buffer
	_, _ = buff.WriteString(msg)
	length := len(ctx)
	var str string
	var as = make([]interface{}, 0, length/2)
	if length > 0 {
		_ = buff.WriteByte('{')
		for i, c := range ctx {
			if i%2 == 0 {
				if i > 0 {
					_ = buff.WriteByte(',')
				}
				str = strings.Replace(fmt.Sprintf("%q", c), "%", "%%", -1)
				if i+1 < length {
					_, _ = buff.WriteString(fmt.Sprintf("%q:%%q", str))
				} else {
					_, _ = buff.WriteString(str)
				}
			} else {
				as = append(as, c)
			}
		}
		_ = buff.WriteByte('}')
	}
	//fmt.Printf("msg:%s\n", buff.String())
	//fmt.Printf("ctx:%d,length:%d\n", len(ctx), length)
	//for i, a := range as {
	//	fmt.Printf("%d>%v\n", i, a)
	//}
	return fmt.Sprintf(buff.String(), as...)
}
func Errorf(msg string, ctx ...interface{}) error {
	return errors.New(Log(msg, ctx...))
}

func Marshal4json(v interface{}) string {
	//json4marshal, err := json.Marshal(v)
	//if err != nil {
	//	return fmt.Sprintf(`{"json.Marshal":"%v","err":"%v"}`, v, err)
	//} else {
	//	return String(json4marshal)
	//}
	return MarshalFn(func() (data []byte, err error) {
		return json.Marshal(v)
	}, func(err error) string {
		return fmt.Sprintf(`{"json.Marshal":"%v","err":"%v"}`, v, err)
	})
}
func MarshalFn(fn4marshal func() (data []byte, err error), marshal4err func(err error) string) (str4marshal string) {
	byte4json, err := fn4marshal()
	if err != nil {
		str4marshal = marshal4err(err)
	} else {
		str4marshal = String(byte4json)
	}
	return
}

func WriteString(sb *strings.Builder, str string, ln4must bool, str4ln string) (cnt int) {
	return WriteData(sb, Slice(str), ln4must, Slice(str4ln))
}
func WriteData(writer io.Writer, data []byte, ln4must bool, data4ln []byte) (cnt int) {
	var err error
	cnt4wrt := 0
	if len(data) > 0 {
		cnt4wrt, err = writer.Write(data)
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	if ln4must || len(data4ln) > 0 {
		cnt4wrt, err = fmt.Fprintln(writer)
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	if len(data4ln) > 0 {
		cnt4wrt, err = writer.Write(data4ln) // it must write twice to avoid [][]byte
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	return
}
func DefinedWriteString(defined Set) (import4jit, go4jit string) {
	if Generated(defined, "DefinedWriteString") {
		return "", ""
	}
	import4jit, go4jit = DefinedBytes2string(defined)
	sb := &strings.Builder{}
	sb.Grow(64)
	WriteString(sb, "", false, import4jit)
	WriteString(sb, "", false, Import(defined, "strings"))
	WriteString(sb, "", false, Import(defined, "io"))
	WriteString(sb, "", false, Import(defined, "fmt"))

	return sb.String(), go4jit + `
func WriteString(sb *strings.Builder, str string, ln4must bool, str4ln string) (cnt int) {
	return WriteData(sb, Slice(str), ln4must, Slice(str4ln))
}
func WriteData(writer io.Writer, data []byte, ln4must bool, data4ln []byte) (cnt int) {
	var err error
	cnt4wrt := 0
	if len(data) > 0 {
		cnt4wrt, err = writer.Write(data)
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	if ln4must || len(data4ln) > 0 {
		cnt4wrt, err = fmt.Fprintln(writer)
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	if len(data4ln) > 0 {
		cnt4wrt, err = writer.Write(data4ln) // it must write twice to avoid [][]byte
		if err != nil {
			panic(err)
		}
		cnt += cnt4wrt
	}
	return
}
`
}
