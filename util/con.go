package util

import "time"

var TRUE = true
var FALSE = false

const (
	CAP4CHANNEL = 256
)

type CloseChannel chan struct{}

const (
	OK    = "ok"
	ERROR = "err"
	// Delimiter4dsv Dsv Delimiter-Separated Values
	Delimiter4dsv = ','
	Quote4dsv     = '\\'
)

const (
	TimeWndSec4pipeWriter = 60
	TimeWnd4pipeWriter    = TimeWndSec4pipeWriter * time.Second
	LineMax4pipeWriter    = 1000000 // 0.5k * 1k * 1k = 0.5g

	Dir4load = "load"

	Dir4loading = "ing"
	Dir4loaded  = "bck"
	Dir4loadErr = "err" // ERROR

	Dir4dataLoad    = "data/" + Dir4load + "/"
	Dir4dataLoading = Dir4dataLoad + Dir4loading // filepath.Join("data", Dir4load, "ing")
	Dir4dataLoaded  = Dir4dataLoad + Dir4loaded
	Dir4dataLoadErr = Dir4dataLoad + Dir4loadErr
)
