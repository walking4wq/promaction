/* // +build go1.17 */

package util

import (
	"fmt"
	"math"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestOS_dir(t *testing.T) {
	root := ".."
	/* // +build go1.17 */
	//des, err := os.ReadDir(root) // undefined: os.ReadDir @ go1.15.15
	//if err != nil {
	//	t.Errorf("os.ReadDir(%s) err:%v", root, err)
	//}
	//t.Logf("os.ReadDir(%s) return [%d]", root, len(des))
	//for i, de := range des {
	//	t.Logf("%d>%v", i, de)
	//}

	file, err := os.Open(root)
	if err != nil {
		t.Fatalf("os.Open(%s) err:%v", root, err)
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	fns, err := file.Readdirnames(-1)
	if err != nil {
		t.Errorf("[%s].Readdirnames(-1) err:%v", root, err)
	}
	t.Logf("[%s].Readdirnames(-1) return [%d]", root, len(fns))
	for i, fn := range fns {
		t.Logf("%d>%v", i, fn)
	}
	file2, err := os.Open(root)
	if err != nil {
		t.Fatalf("os.Open(%s) err:%v", root, err)
	}
	defer func(file2 *os.File) {
		_ = file2.Close()
	}(file2)
	dirs, err := file2.Readdir(-1)
	if err != nil {
		t.Errorf("[%s].Readdir(-1) err:%v", root, err)
	}
	t.Logf("[%s].Readdir(-1) return [%d]", root, len(dirs))
	for i, dir := range dirs {
		t.Logf("%d>%v", i, dir)
	}
}

func TestFile_ReadLine(t *testing.T) {
	filename := "./file_test.go"
	err := ReadLine(filename, nil, func(line int, data4line []byte, last bool) (brk bool, err error) {
		t.Logf("%d:%t\t>%s:%v", line, last, String(data4line), data4line)
		return line > 10, nil
	})
	if err != nil {
		t.Errorf("ReadLine(%s) err:%v", filename, err)
	} else {
		t.Logf("ReadLine(%s) success", filename)
	}
}

func TestFile_dir(t *testing.T) {
	root := ".."
	dirnames, err := Readdirnames(root, -1, &TRUE)
	if err != nil {
		t.Errorf("Readdirnames(%s,-1,true) err:%v", root, err)
	}
	t.Logf("Readdirnames(%s,-1,true) return:[%d]string", root, len(dirnames))
	for i, dirname := range dirnames {
		t.Logf("%d>%s", i, dirname)
	}
	dirnames, err = Readdirnames(root, -1, &FALSE)
	if err != nil {
		t.Errorf("Readdirnames(%s,-1,false) err:%v", root, err)
	}
	t.Logf("Readdirnames(%s,-1,false) return:[%d]string", root, len(dirnames))
	for i, dirname := range dirnames {
		t.Logf("%d>%s", i, dirname)
	}
	dirnames, err = Readdirnames(root, -1, nil)
	if err != nil {
		t.Errorf("Readdirnames(%s,-1,nil) err:%v", root, err)
	}
	t.Logf("Readdirnames(%s,-1,nil) return:[%d]string", root, len(dirnames))
	for i, dirname := range dirnames {
		t.Logf("%d>%s", i, dirname)
	}
	var dirnames2 []string
	for i := 0; i < 10; i++ {
		dirnames2, err = Readdirnames(root, -1, nil)
		if err != nil {
			t.Errorf("Readdirnames(%s,-1,nil) err:%v", root, err)
		}
		if !reflect.DeepEqual(dirnames, dirnames2) {
			dirname, dirname2 := "", ""
			for j := 0; float64(j) < math.Max(float64(len(dirnames)), float64(len(dirnames2))); j++ {
				if j < len(dirnames) {
					dirname = dirnames[j]
				}
				if j < len(dirnames2) {
					dirname2 = dirnames2[j]
				}
				t.Logf("%d>[%s]\t\t\t[%s]", i, dirname, dirname2)
			}
		}
	}
	dirnames, err = Readdirnames(root, 2, nil)
	if err != nil {
		t.Errorf("Readdirnames(%s,2,nil) err:%v", root, err)
	}
	t.Logf("Readdirnames(%s,2,nil) return:[%d]string", root, len(dirnames))
	for i, dirname := range dirnames {
		t.Logf("%d>%s", i, dirname)
	}

	dirs, err := Readdir(root, -1, &TRUE)
	if err != nil {
		t.Errorf("Readdir(%s,-1,true) err:%v", root, err)
	}
	t.Logf("Readdir(%s,-1,true) return:[%d]os.FileInfo", root, len(dirs))
	for i, dir := range dirs {
		t.Logf("%d>%v", i, dir)
	}
	dirs, err = Readdir(root, -1, &FALSE)
	if err != nil {
		t.Errorf("Readdir(%s,-1,false) err:%v", root, err)
	}
	t.Logf("Readdir(%s,-1,false) return:[%d]os.FileInfo", root, len(dirs))
	for i, dir := range dirs {
		t.Logf("%d>%v", i, dir)
	}
	dirs, err = Readdir(root, -1, nil)
	if err != nil {
		t.Errorf("Readdir(%s,-1,nil) err:%v", root, err)
	}
	t.Logf("Readdir(%s,2,nil) return:[%d]os.FileInfo", root, len(dirs))
	for i, dir := range dirs {
		t.Logf("%d>%v", i, dir)
	}
	dirs, err = Readdir(root, 2, nil)
	if err != nil {
		t.Errorf("Readdir(%s,2,nil) err:%v", root, err)
	}
	t.Logf("Readdir(%s,2,nil) return:[%d]os.FileInfo", root, len(dirs))
	for i, dir := range dirs {
		t.Logf("%d>%v", i, dir)
	}
}

func TestFile_ReadLimit(t *testing.T) {
	filename := "./file_test.go"
	limit, count := 3, 4
	err := ReadLimit(filename, nil, limit, count, func(idx int, buff []byte, more bool) (brk bool, cnt uint64, err error) {
		cnt = uint64(idx % limit)
		len4buf := uint64(len(buff))
		if cnt > len4buf {
			cnt = len4buf
		}
		t.Logf("%d:%t\t>%s:%v\nfetch:%d\t:%s:%v", idx, more, String(buff), buff, cnt, String(buff[0:cnt]), buff[0:cnt])
		// brk = idx > 10
		return
	})
	if err != nil {
		t.Errorf("ReadLimit(%s) err:%v", filename, err)
	} else {
		t.Logf("ReadLimit(%s) success", filename)
	}
}

func TestFile_base4Writer4pipe(t *testing.T) {
	wrt4pipe := &Writer4pipe{
		Writer4pipeMeta: &Writer4pipeMeta{Wnd4time: time.Second * 10, Max4line: 3, Naming: func() (dir, key, seq string) {
			return "data/test", "", ""
		}}, Println: true, Gzip: true,
	}
	var wrt int
	var err error
	var data []byte
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		data = Slice(fmt.Sprintf("%d", i))
		wrt, err = wrt4pipe.Write(data)
		if err != nil {
			t.Fatalf("%v.Write(%d) err:%d:%v", wrt4pipe, i, wrt, err)
		}
		t.Logf("after write:%q:%v", data, data)
	}
	wrt, err = wrt4pipe.Write(nil)
	if err != nil {
		t.Fatalf("%v.Write close err:%d:%v", wrt4pipe, wrt, err)
	}
}

func TestFile_Writer4pipe(t *testing.T) {
	wrt4pipe := &Writer4pipe{Writer4pipeMeta: &Writer4pipeMeta{
		Wnd4time: time.Second * 10, Max4line: 3, Naming: func() (dir, key, seq string) {
			return "data/test", "", ""
		}}, Println: true, Gzip: true,
	}
	rcv, wg := wrt4pipe.Run()
	var data []byte
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		data = Slice(fmt.Sprintf("%d", i))
		rcv <- data
		t.Logf("after write:%q:%v", data, data)
	}
	close(rcv)
	wg.Wait()
	t.Logf("end of test")
}

func TestFile_empty4Writer4pipe(t *testing.T) {
	wrt4pipe := &Writer4pipe{
		Writer4pipeMeta: &Writer4pipeMeta{Wnd4time: time.Second * 3, Max4line: 3, Naming: func() (dir, key, seq string) {
			return "data/test", "", ""
		}}, Println: true, Gzip: true,
	}
	rcv, wg := wrt4pipe.Run()
	rcv <- []byte{'a'}
	var data []byte
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		rcv <- data
		t.Logf("after write:%q:%v", data, data)
	}
	close(rcv)
	wg.Wait()
	t.Logf("end of test")
}

func TestFile_closure(t *testing.T) {
	cnt := 3
	namings := make([]Naming4pipe, 0, 3)
	for i := 0; i < cnt; i++ {
		key_ := fmt.Sprintf("%d", i)
		namings = append(namings, func() (dir, key, seq string) {
			//key = key_
			//seq = fmt.Sprintf("%d", i)
			return "", key_, fmt.Sprintf("%d", i)
		})
	}
	for i, naming := range namings {
		_, key, seq := naming()
		if key != fmt.Sprintf("%d", i) {
			t.Errorf("namings[%d] return key[%s] seq[%s] failure!", i, key, seq)
		} else {
			t.Logf("namings[%d] return key[%s] seq[%s]!", i, key, seq)
		}
	}
}

func TestFile_LRUWriter4pipe(t *testing.T) {
	wp := NewLRUWriter4pipe(3)
	defer func() {
		wp.Close()
		t.Logf("end of close")
	}()
	for i := 0; i < 6; i++ {
		key_ := fmt.Sprintf("%d", i)
		for j := 0; j < 3; j++ {
			wp.Write(&Writer4pipeMeta{0, 0, func() (dir, key, seq string) {
				dir = "data/test"
				key = key_
				seq = time.Now().Format(FMT4TIMESTAMPZ)
				return
			}}, Slice(fmt.Sprintf("%d/%d", j, i)))
		}
	}
	t.Logf("end of write data")
}
