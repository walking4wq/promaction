package svr

import (
	"fmt"
	log "github.com/inconshreveable/log15"
	"gitlab.com/walking4wq/promaction/storage"
	"gitlab.com/walking4wq/promaction/util"
	"gitlab.com/walking4wq/promaction/web"
	"path/filepath"
	"sort"
	"sync"
	"time"
)

func Start4dev(opts *web.Options) (qq <-chan struct{}, err error) {
	cc := make(util.CloseChannel)
	qq = cc
	go func() {
		defer func() {
			close(cc)
		}()
		prefix := opts.Prefix4cluster
		var wg sync.WaitGroup
		var found bool
		var job, job4prev *util.Job
		reset := true
		tick := time.Tick(500 * time.Millisecond) // opts.
		buff4cb := util.CAP4CHANNEL               // opts.
		callback := make(chan *util.Job, buff4cb)

		wrt4pipe := util.NewLRUWriter4pipe(2) // opts.
		rcv4arch, wg4arch := wrt4pipe.Run()
		for {
			select {
			case <-cc:
				wg.Wait()
				close(rcv4arch)
				wg4arch.Wait()
				return
			case <-tick:
				jobs := make(map[string]*util.Job)
				err = opts.Storage.ScanJob(prefix, func(cursor uint64, match, key string, offset int) (skip *bool) {
					// ref: https://redis.io/commands/scan/
					// It is up to the application to handle the case of duplicated elements
					if _, found = jobs[key]; found {
						return
					}
					job, err = opts.Storage.GetJob(key)
					if err != nil {
						log.Error("device server scan key for get job", "key", key,
							"cursor", cursor, "match", match, "offset", offset, "err", err)
						err = nil
						return
					}
					if reset && job.Archived != nil && !*job.Archived {
						job.Archived = nil
					}
					jobs[key] = job
					return
				})
				if err != nil {
					log.Error("device server scan", "prefix", prefix, "err", err)
					time.Sleep(5 * time.Second) // wait for storage recover
					continue
				}
				reset = false // just scan once for all not archived job
				job4trg := make([]*util.Job, 0, len(jobs))
				var trg bool
				var um4next int64
				for _, job = range jobs {
					now := time.Now()
					trg, um4next, err = job.Triggering(now)
					if err != nil {
						job.Info = fmt.Sprintf("%v. %s", err, job.Info)
						// job.UnixMilli4trg = um4next // update the current trigger time
						job.Archived = &util.FALSE
						stop(opts.Storage, prefix, job, rcv4arch)
						continue
					}
					if !trg {
						if um4next < 0 {
							job.Archived = &util.TRUE
							stop(opts.Storage, prefix, job, rcv4arch)
						} else {
							if job.UnixMilli4trg != um4next {
								job.UnixMilli4trg = um4next
								stop(opts.Storage, prefix, job, rcv4arch)
							}
						}
						continue
					}
					for key4prev := range job.Ids4prev {
						job4prev, found = jobs[opts.Storage.Key4devJob(prefix, key4prev)]
						if !found {
							continue
						}
						if job4prev.Triggered(now) {
							goto NEXT_JOB
						}
					}
					job.UnixMilli4trg = um4next // update the current trigger time
					job4trg = append(job4trg, job)
				NEXT_JOB:
				}
				if len(job4trg) < 1 {
					continue
				}
				log.Debug("device server scan", "prefix", prefix, "scanned", len(jobs), "trigger", len(job4trg))
				sort.Slice(job4trg, func(i, j int) bool {
					return job4trg[i].Sort(job4trg[j], true) < 0
				})
				for _, job = range job4trg {
					job.Archived = &util.FALSE
					if !stop(opts.Storage, prefix, job, rcv4arch) {
						continue
					}
					wg.Add(1)                    // before start goroutine
					go func(job4trg *util.Job) { // TODO need pooling goroutines
						defer func() {
							wg.Done() // stop gracefully
						}()
						log.Debug("device server trigger", "job", util.Marshal4json(job4trg))
						// TODO call service for job
						job4trg.UnixMilli4exe = time.Now().UnixMilli()
						if job4trg.UnixMilli4trg < 0 {
							job4trg.Archived = &util.TRUE
						} else {
							job4trg.Archived = nil
						}
						callback <- job4trg
					}(job)
				}
			case job = <-callback:
				stop(opts.Storage, prefix, job, rcv4arch)
			}
		}
	}()
	return
}

func stop(stg storage.Storage, prefix string, job *util.Job, rcv chan<- *util.LRUWriter4pipeData) (stopped bool) {
	var err error
	if job.Archived != nil && *job.Archived {
		var cnt int64
		cnt, err = stg.DelJob(prefix, job.Id)
		if cnt != 1 || err != nil {
			log.Error("device server storage del job failed", filepath.Join("job", prefix), util.Marshal4json(job),
				"cnt", cnt, "err", err)
			job.Info = fmt.Sprintf("del err:%d,%v. %s", cnt, err, job.Info)
		}
		log.Debug("device server archived job", "job", util.Marshal4json(job))
		rcv <- &util.LRUWriter4pipeData{&util.Writer4pipeMeta{
			Naming: func() (dir, key, seq string) {
				return util.Dir4dataLoading, "job4dev", time.Now().Format(util.FMT4TIMESTAMP)
			},
		}, job.DsvMarshal(0, 0)}

	} else {
		err = stg.SetJob(prefix, job)
		if err != nil {
			log.Error("device server storage set job", filepath.Join("job", prefix), util.Marshal4json(job), "err", err)
		}
	}
	return err == nil
}
