package v1

import (
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt"
	log "github.com/inconshreveable/log15"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/walking4wq/promaction/model"
	"gitlab.com/walking4wq/promaction/service"
	"gitlab.com/walking4wq/promaction/util"
	"io/ioutil"
	"net/http"
)
import "gitlab.com/walking4wq/promaction/storage"

func Register(web *echo.Echo, stg storage.Storage, pvt []byte, prefix4cluster string) {
	prefix := web.Group("/pma/api/v1/dev")
	// just for simple test
	prefix.GET("/list", func(c echo.Context) error {
		var json4req []byte
		json4req, err := ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return c.String(http.StatusOK, fmt.Sprintf("failed ioutil.ReadAll body of request:%v", err))
		}
		paging := &model.Req4paging{}
		err = json.Unmarshal(json4req, paging)
		if err != nil {
			//return c.JSON(http.StatusOK, fmt.Sprintf("failed json.Unmarshal body of request:%v", err))
			log.Debug("failed json.Unmarshal body of request", "json4req", util.String(json4req), "err", err)
		}
		siz4page := paging.Siz
		idx4page := paging.Idx
		var asc *bool
		if len(paging.Order) > 0 {
			if paging.Order[0].Asc {
				asc = &util.TRUE
			} else {
				asc = &util.FALSE
			}
		}
		// var data []*model.Device
		rsp := model.Rsp4device{Req4paging: paging}
		paging.Cnt, paging.Idx, err = stg.ScanDevice("*", siz4page, idx4page, asc,
			func(key4match, key string, offset int, dev *model.Device, err error) (brk bool) {
				log.Debug("web handler", "req", c.Request(), key4match, key, "offset", offset,
					"dev", dev, "err", err)
				if err == nil {
					// dev.Secret, dev.Token4refresh, dev.Token4access = "*", "*", "*"
					dev.Token4refresh = "*"
					rsp.Data = append(rsp.Data, dev)
				}
				return
			})
		if err != nil {
			return err
		}
		//return c.String(http.StatusOK, util.Marshal4json(data))
		return c.JSON(http.StatusOK, rsp)
	})
	prefix.GET("/sample4dfa", func(ctx4http echo.Context) error {
		html4img, err := service.Test4dfa()
		if err != nil {
			return err
		}
		return ctx4http.HTML(http.StatusOK, html4img)
	})

	prefix.POST("/token", func(c echo.Context) (err error) {
		id := c.FormValue("id")
		pwd := c.FormValue("pwd")
		msg := util.Msg{
			Name: "token4device",
			Code: util.OK,
		}
		var token *model.Rsp4token
		token, err = service.Token(id, pwd, stg, pvt)
		if err != nil {
			msg.Code = util.ERROR
			msg.Info = err.Error()
		} else {
			msg.Code = util.OK
			msg.Rsp = util.Slice(util.Marshal4json(token))
		}
		//return c.String(http.StatusOK, util.Marshal4json(msg))
		return c.JSON(http.StatusOK, msg)
	})
	grp := prefix.Group("/")
	// ref by: http://echo.topgoer.com/%E8%8F%9C%E8%B0%B1/JWT.html
	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     service.NewDeviceClaims(),
		SigningKey: pvt, // private key for signe
	}
	grp.Use(middleware.JWTWithConfig(config))
	grp.POST("rpc", func(c echo.Context) (err error) {
		// github.com/labstack/echo/v4@v4.9.0/middleware/jwt.go:137 DefaultJWTConfig.ContextKey:"user"
		token := c.Get("user").(*jwt.Token)
		//claims := user.Claims.(*jwtDeviceClaims)
		//name := claims.Name
		//return c.String(http.StatusOK, util.Marshal4json(token.Claims))\
		// return c.JSON(http.StatusOK, token.Claims)
		log.Debug("post", "req", c.Request(), "token", util.Marshal4json(token.Claims))
		msg := &util.Msg{Name: "rpc4device"}
		var json4req []byte
		json4req, err = ioutil.ReadAll(c.Request().Body)
		if err != nil {
			msg.Code = util.ERROR
			msg.Info = fmt.Sprintf("failed ioutil.ReadAll body of request:%v", err)
			return c.JSON(http.StatusOK, msg)
		}
		err = json.Unmarshal(json4req, msg)
		if err != nil {
			msg.Code = util.ERROR
			msg.Info = fmt.Sprintf("failed json.Unmarshal body of request:%v", err)
			return c.JSON(http.StatusOK, msg)
		}
		var rsp *util.Msg
		rsp, err = service.Rpc4dev(token.Claims, msg, c, stg, prefix4cluster)
		if err != nil {
			msg.Code = util.ERROR
			msg.Info = fmt.Sprintf("failed service.Rpc4dev call:%v", err)
			return c.JSON(http.StatusOK, msg)
		}
		if rsp != nil {
			return c.JSON(http.StatusOK, rsp)
		}
		return
	})
}
