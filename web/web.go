package web

import (
	"crypto/tls"
	"fmt"
	log "github.com/inconshreveable/log15"
	"github.com/labstack/echo-contrib/prometheus"
	toolkit_web "github.com/prometheus/exporter-toolkit/web"
	"github.com/prometheus/prometheus/web"
	"gitlab.com/walking4wq/promaction/storage"
	apiv1 "gitlab.com/walking4wq/promaction/web/api/v1"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
	//"github.com/casbin/casbin/v2"
	//casbin_mw "github.com/labstack/echo-contrib/casbin"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
)

type Options struct {
	web.Options
	storage.Storage
	SigningKey     []byte
	Prefix4cluster string
	// other options, such as: middleware, routes
}

type Web struct {
	*echo.Echo
	*Options
	//sync.WaitGroup
	quitCh chan struct{}
}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hi, Promaction!")
}

func New(opts *Options) (hdl *Web) {
	// Echo instance
	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// Enable metrics middleware
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
	// ref: http://echo.topgoer.com/%E4%B8%AD%E9%97%B4%E4%BB%B6/Gzip.html
	//e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
	//	Level: 5,
	//}))
	//ce, err := casbin.NewEnforcer("auth_model.conf", "auth_policy.csv")
	//if err != nil {
	//}
	//e.Use(casbin_mw.Middleware(ce))
	// ref: http://echo.topgoer.com/%E6%8C%87%E5%8D%97/%E9%94%99%E8%AF%AF%E5%A4%84%E7%90%86.html
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		c.Logger().Error(err)
		code := http.StatusInternalServerError
		if he, ok := err.(*echo.HTTPError); ok {
			code = he.Code
		}
		errorPage := fmt.Sprintf("%d.html", code)
		if err4file := c.File(errorPage); err4file != nil {
			c.Logger().Error(fmt.Sprintf("failure load errorPage[%s] err:%v", errorPage, err4file))
			err4rsp := c.String(code, err.Error())
			if err4rsp != nil {
				c.Logger().Error(fmt.Sprintf("failure rsp [%d,%v] err:%v", code, err, err4rsp))
			}
		}
	}
	e.GET("/", hello)
	hdl = &Web{e, opts, make(chan struct{})}
	if len(opts.SigningKey) < 1 {
		opts.SigningKey = []byte("password for promaction web gateway")
	}
	// Routes
	apiv1.Register(e, opts.Storage, opts.SigningKey, opts.Prefix4cluster)
	return
}

func (this *Web) Run(webConfig string) (err error) {
	var cfg *toolkit_web.Config
	cfg, err = getConfig(webConfig)
	if err != nil {
		log.Warn("failure load webConfig", "webConfig", webConfig, "web", this, "err", err)
		err = nil
	}
	go func() {
		defer func() {
			// this.e.Shutdown(ctx)
			close(this.quitCh)
			if err != nil {
				log.Error("web server quit error", "err", err)
			}
		}()
		// Start server
		//e.Logger.Fatal(e.Start(":1323")) // e.StartTLS(":1323", nil, nil)
		if cfg == nil || (cfg.TLSConfig.TLSCertPath == "" && cfg.TLSConfig.TLSKeyPath == "") {
			log.Info("web server start", "ListenAddress", this.Options.ListenAddress)
			err = this.Start(this.Options.ListenAddress)
		} else {
			// ref: github.com/labstack/echo/v4@v4.9.0/_fixture/certs/README.md:2
			// openssl req -x509 -newkey rsa:4096 -sha256 -days 9999 -nodes \
			//  -keyout key.pem -out cert.pem -subj "/CN=localhost" \
			//  -addext "subjectAltName=DNS:localhost,IP:127.0.0.1,IP:::1"
			log.Info("web server start tls", "ListenAddress", this.Options.ListenAddress,
				cfg.TLSConfig.TLSCertPath, cfg.TLSConfig.TLSKeyPath)
			// ref: http://echo.topgoer.com/%E8%8F%9C%E8%B0%B1/HTTP2.html
			err = this.StartTLS(this.Options.ListenAddress, cfg.TLSConfig.TLSCertPath, cfg.TLSConfig.TLSKeyPath)
		}
	}()

	return
}

// getConfig ref: github.com/prometheus/exporter-toolkit@v0.7.1/web/tls_config.go:66
func getConfig(configPath string) (*toolkit_web.Config, error) {
	content, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, err
	}
	// content ref:
	// https://prometheus.io/docs/prometheus/latest/configuration/https/
	// https://github.com/prometheus/prometheus/blob/release-2.38/documentation/examples/web-config.yml
	_ = `
# TLS and basic authentication configuration example.
#
# Additionally, a certificate and a key file are needed.
tls_server_config:
  cert_file: server.crt
  key_file: server.key

# Usernames and passwords required to connect to Prometheus.
# Passwords are hashed with bcrypt: https://github.com/prometheus/exporter-toolkit/blob/master/docs/web-configuration.md#about-bcrypt
basic_auth_users:
  alice: $2y$10$mDwo.lAisC94iLAyP81MCesa29IzH37oigHC/42V2pdJlUprsJPze
  bob: $2y$10$hLqFl9jSjoAAy95Z/zw8Ye8wkdMBM8c5Bn1ptYqP/AXyV0.oy0S8m
`
	c := &toolkit_web.Config{
		TLSConfig: toolkit_web.TLSStruct{
			MinVersion:               tls.VersionTLS12,
			MaxVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
		},
		HTTPConfig: toolkit_web.HTTPStruct{HTTP2: true},
	}
	err = yaml.UnmarshalStrict(content, c)
	//if err == nil {
	//	err = validateHeaderConfig(c.HTTPConfig.Header)
	//}
	c.TLSConfig.SetDirectory(filepath.Dir(configPath))
	return c, err
}

// Quit returns the receive-only quit channel.
func (this *Web) Quit() <-chan struct{} {
	return this.quitCh
}
