package service

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/goccy/go-graphviz"
	log "github.com/inconshreveable/log15"
	"github.com/labstack/echo/v4"
	"github.com/looplab/fsm"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/walking4wq/promaction/model"
	"gitlab.com/walking4wq/promaction/storage"
	"gitlab.com/walking4wq/promaction/util"
	"net/http"
	"reflect"
	"time"
)

// jwtDeviceClaims is just base64, and no secret
type jwtDeviceClaims struct {
	*jwt.StandardClaims
	// *model.Device // secret unsafe in here
	Code string `json:"code"`
	Name string `json:"name,omitempty"`
}

func NewDeviceClaims() (claims *jwtDeviceClaims) {
	return &jwtDeviceClaims{}
}

// Token ref by: http://echo.topgoer.com/%E8%8F%9C%E8%B0%B1/JWT.html
func Token(id, pwd string, stg storage.Storage, pvt []byte) (token4access *model.Rsp4token, err error) {
	var dev *model.Device
	dev, err = stg.GetDevice(id)
	if err != nil {
		return
	}
	if pwd != dev.Token4refresh {
		err = fmt.Errorf("failed to check refresh token for device")
		return
	}
	now := time.Now()
	ttl := time.Hour * 2
	expiresAt := now.Add(ttl)
	// Set custom claims
	claims := &jwtDeviceClaims{
		&jwt.StandardClaims{
			ExpiresAt: expiresAt.Unix(),
		}, dev.Code, dev.Name,
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	var tkn string
	// Generate encoded token and send it as response.
	tkn, err = token.SignedString(pvt)
	if err != nil {
		return
	}
	token4access = &model.Rsp4token{
		tkn, expiresAt.UnixMilli(), ttl.Milliseconds(),
	}
	return
}

var rpc4dev util.Services

func Rpc4dev(token jwt.Claims, req *util.Msg, ctx4http echo.Context, stg storage.Storage, prefix4cluster string) (rsp *util.Msg, err error) {
	claims, ok := token.(*jwtDeviceClaims)
	if !ok {
		err = fmt.Errorf("failed cast token[%v].(*jwtDeviceClaims) to %v", token, reflect.TypeOf(token))
		return
	}
	rsp, _ = rpc4dev.Call(req, claims.Code, ctx4http, stg, prefix4cluster)
	return
}

func init() {
	rpc4dev = util.Services{
		Mapper: map[string]util.Service{
			"hi":      hi,
			"job":     job,
			"dfa4rpc": dfa4rpc,
			// TODO add service func for dev rpc
		},
		Cache: map[string]*util.Msg{},
		HistogramVec: promauto.NewHistogramVec(prometheus.HistogramOpts{
			Name:    "rpc4dev_call_duration_seconds",
			Help:    "Duration of call dev rpc service.",
			Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 3, 4, 5, 7, 10}, // prometheus.DefBuckets,
		}, []string{"name"}),
	}
	//var err error
	//err = prometheus.Register(rpc4dev.HistogramVec)
	//if err != nil { // duplicate metrics collector registration attempted
	//	log.Warn("prometheus.Register", "rpc4devCallDuration", rpc4dev.HistogramVec, "err", err)
	//}
}

func hi(req *util.Msg, env ...interface{}) (rsp *util.Msg, err error) {
	log.Debug("device service hi", "req", req, "env", env)
	rsp = req
	code := env[0].(string)
	req.Info = fmt.Sprintf("hi for [%s]%v", code, env)
	return
}

type Req4job struct {
	Prefix string      `json:"prefix,omitempty"`
	Jobs   []*util.Job `json:"jobs,omitempty"`
}

func Job(stg storage.Storage, req *Req4job) (cnt int, err error) {
	cnt, err = stg.SetJobs(req.Prefix, req.Jobs)
	if err != nil {
		err = fmt.Errorf("job[%d/%d] %v", cnt, len(req.Jobs), err)
	}
	return
}

func job(req *util.Msg, env ...interface{}) (rsp *util.Msg, err error) {
	log.Debug("device service job", "req", util.Marshal4json(req), "env", env)
	code := env[0].(string)
	stg := env[2].(storage.Storage)
	prefix := env[3].(string)
	args := &Req4job{}
	err = json.Unmarshal(req.Req, args)
	if err != nil {
		err = fmt.Errorf("json.Unmarshal job err:%v by:%s", err, util.String(req.Req))
		return
	}
	if args.Prefix == "" {
		args.Prefix = prefix
	}
	for _, job := range args.Jobs {
		job.Note = fmt.Sprintf("crt by [%s].%s", code, job.Note)
	}
	var cnt int
	cnt, err = Job(stg, args)
	if err != nil {
		return
	}
	rsp = req
	//rsp.Rsp = util.Slice(util.Marshal4json(args))
	rsp.Info = fmt.Sprintf("job[%d/%d] has set", cnt, len(args.Jobs))
	return
}

// dfa4rpc
//  Deterministic finite automata
//  not is Nondeterministic finite automata
func dfa4rpc(req *util.Msg, env ...interface{}) (rsp *util.Msg, err error) {
	html4img, err := Test4dfa() // TODO just for test
	if err != nil {
		return
	}
	_ = req
	ctx4http := env[1].(echo.Context)
	//wrt, err := ctx4http.Response().Write(util.Slice(html4img))
	//if err != nil {
	//	err = fmt.Errorf("rsp write err:%d,%v for[%s]", wrt, err, html4img)
	//	return
	//}
	err = ctx4http.HTML(http.StatusOK, html4img)
	return
}

func Test4dfa() (html4img string, err error) {
	dfa := fsm.NewFSM(
		"idle",
		fsm.Events{
			{Name: "scan", Src: []string{"idle"}, Dst: "scanning"},
			{Name: "working", Src: []string{"scanning"}, Dst: "scanning"},
			{Name: "situation", Src: []string{"scanning"}, Dst: "scanning"},
			{Name: "situation", Src: []string{"idle"}, Dst: "idle"},
			{Name: "finish", Src: []string{"scanning"}, Dst: "idle"},
		}, nil,
	)
	gv := fsm.Visualize(dfa)
	graph, err := graphviz.ParseBytes(util.Slice(gv))
	if err != nil {
		err = fmt.Errorf("graphviz.ParseBytes for %v err:%v", dfa, err)
		return
	}
	g := graphviz.New()
	var buf bytes.Buffer
	if err = g.Render(graph, graphviz.PNG, &buf); err != nil {
		err = fmt.Errorf("graphviz.Render png for %v err:%v", dfa, err)
		return
	}
	pic4base64 := base64.StdEncoding.EncodeToString(buf.Bytes())
	html4img = fmt.Sprintf(`<img src="data:image/png;base64,%s"/>`, pic4base64)
	return

}
