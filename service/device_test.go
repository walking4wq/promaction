package service

import (
	"bytes"
	"encoding/base64"
	"github.com/goccy/go-graphviz"
	"github.com/looplab/fsm"
	"gitlab.com/walking4wq/promaction/util"
	"testing"
)

// Finite Automata

func TestDevice_dfa(t *testing.T) {
	dfa := fsm.NewFSM(
		"idle",
		fsm.Events{
			{Name: "scan", Src: []string{"idle"}, Dst: "scanning"},
			{Name: "working", Src: []string{"scanning"}, Dst: "scanning"},
			{Name: "situation", Src: []string{"scanning"}, Dst: "scanning"},
			{Name: "situation", Src: []string{"idle"}, Dst: "idle"},
			{Name: "finish", Src: []string{"scanning"}, Dst: "idle"},
		},
		fsm.Callbacks{
			"scan": func(e *fsm.Event) {
				t.Logf("after_scan: " + e.FSM.Current())
			},
			"working": func(e *fsm.Event) {
				t.Logf("working: " + e.FSM.Current())
			},
			"situation": func(e *fsm.Event) {
				t.Logf("situation: " + e.FSM.Current())
			},
			"finish": func(e *fsm.Event) {
				t.Logf("finish: " + e.FSM.Current())
			},
		},
	)
	t.Logf(dfa.Current())
	err := dfa.Event("scan")
	if err != nil {
		t.Logf("err:%v", err)
	}
	t.Logf("1:" + dfa.Current())
	err = dfa.Event("working")
	if err != nil {
		t.Logf("err:%v", err)
	}
	t.Logf("2:" + dfa.Current())
	err = dfa.Event("situation")
	if err != nil {
		t.Logf("err:%v", err)
	}
	t.Logf("3:" + dfa.Current())
	err = dfa.Event("finish")
	if err != nil {
		t.Logf("err:%v", err)
	}
	t.Logf("4:" + dfa.Current())

	gv := fsm.Visualize(dfa)
	t.Logf("Graphviz:\n%s", gv)

	g := graphviz.New()
	graph, err := graphviz.ParseBytes(util.Slice(gv))
	if err != nil {
		t.Errorf("graphviz.ParseBytes err:%v", err)
	}
	// 1. write encoded PNG data to buffer
	var buf bytes.Buffer
	if err = g.Render(graph, graphviz.PNG, &buf); err != nil {
		t.Errorf("Render err:%v", err)
	}
	// 2. get as image.Image instance
	image, err := g.RenderImage(graph)
	if err != nil {
		t.Fatalf("RenderImage err:%v", err)
	}
	_ = image
	// 3. write to file directly
	if err = g.RenderFilename(graph, graphviz.PNG, "../test4dot.gv.png"); err != nil {
		t.Fatalf("RenderFilename err:%v", err)
	}
	pic4base64 := base64.StdEncoding.EncodeToString(buf.Bytes())
	t.Logf(`<img src="data:image/png;base64,%s"/>`, pic4base64)
	// https://blog.csdn.net/qq_25987491/article/details/80259767
}
