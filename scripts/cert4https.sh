#!/bin/bash

mkdir -p ../conf/.ssh/

openssl req -x509 -newkey rsa:4096 -sha256 -days 9999 -nodes \
 -keyout ../conf/.ssh/key.pem -out ../conf/.ssh/cert.pem -subj "/CN=walking4wq-promaction" \
 -addext "subjectAltName=DNS:promaction,IP:127.0.0.1,IP:::1"


openssl x509 -in ../conf/.ssh/cert.pem -text


