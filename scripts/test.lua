local num_arg = #ARGV
local key = ''
local val = ''
-- local replay = {}
local cnt = 0
for i = 1, num_arg do
  if( i%2 == 1 ) then
    key = ARGV[i]
  else
    val = ARGV[i]
    -- redis.call('SET', key, val)
    local reply = redis.pcall('SET', key, val)
    if reply['err'] ~= nil then
      return redis.error_reply('set ' .. key .. ' ' .. val .. ' err:' .. replay['err'])
    end
    cnt = cnt + 1
  end
end

-- return cnt
return redis.status_reply('' .. cnt)


