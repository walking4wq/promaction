package model

import "gitlab.com/walking4wq/promaction/util"

type History struct {
	LogId          int64  `json:"log_id,omitempty"`
	EditorId       string `json:"editor_id,omitempty"`
	UnixMilli4edit int64  `json:"unix_milli4edit,omitempty"`
	Tips           string `json:"tips,omitempty"`
}

//type Rsp4api struct {
//	Code        string `json:"code,omitempty"`        // An enumerated success or error code for machine use.
//	Details     string `json:"details,omitempty"`     // A human-readable description of the code.
//	Description string `json:"description,omitempty"` // The human-readable description of the error.
//}

type Rsp4token struct {
	// RefreshToken string `json:"refresh_token,omitempty"`
	// AccessToken  string `json:"access_token,omitempty"`
	// TokenType    string `json:"token_type,omitempty"`
	// ExpiresIn int64 `json:"expires_in,omitempty"`
	// ExpiresAt int64 `json:"expires_at,omitempty"`

	Token             string `json:"token,omitempty"`
	UnixMilli4expires int64  `json:"unix_milli4expires,omitempty"`
	Milliseconds4ttl  int64  `json:"milliseconds4ttl,omitempty"`
}

type Req4paging struct {
	util.Page `json:"page,omitempty"`
	Order     []*util.Sort `json:"order,omitempty"`
}
