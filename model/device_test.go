package model

import (
	"encoding/json"
	"gitlab.com/walking4wq/promaction/util"
	"testing"
)

func TestModel_base(t *testing.T) {
	dev := &Device{
		Code:          "1903021818580123450",
		Name:          "sw-001",
		Token4refresh: "123123",
		Attrs: Attrs{"ip": "10.45.121.88", "port": 2208, "user": "root", "pwd": "root11",
			"community": "public", "bandwidth_utilization": 0.1, "ifInOctets": 123456, "ts": 20221003133601001},
		DeviceProfile: &DeviceProfile{
			Id: 1903021818580123450, Name: "host",
			Attrs: util.Set{"ip": util.Sizeof0, "port": util.Sizeof0, "user": util.Sizeof0, "pwd": util.Sizeof0},
			Pri:   1000, Archived: nil,
			Transport: map[string]Attrs{
				"ssh":    {"ip": util.Sizeof0, "port": 22, "user": nil, "pwd": nil},
				"snmpv2": {"ip": util.Sizeof0, "port": 161, "community": nil},
			},
			Rules: []*RuleChain{
				{Id: 1903021818580123450, Name: "routing switch", RuleNode: &RuleNode{
					Id:     1903021818580123450,
					Source: json.RawMessage(`{}`),
					Rpc4device: &Rpc4device{
						Id: 0, Rpc: &util.Job{
							Cron: "0 2 * * *", Retry4max: 3, Id4sync: "1903021818580123450",
						}, Dfa4rpc: &Dfa4rpc{
							Protocol: "ssh", Init: &Command{
								Name: "ssh login", Protocol: "ssh",
								Scr4args: &JsonConverter{Lang: "js", Script: ``},
								Msg: &util.Msg{
									Name: "ssh",
									Req:  json.RawMessage(`{}`),
									Rsp:  json.RawMessage(`{}`),
								},
							},
							Trans: map[string]*TransCmd{
								"login": {"nmap": {Command: &Command{
									Name: "searching new route", Protocol: "ssh",
									Scr4args: &JsonConverter{Lang: "js", Script: ``},
									Msg: &util.Msg{
										Name: "ssh",
										Req:  json.RawMessage(`{}`),
										Rsp:  json.RawMessage(`{}`),
									},
								}}},
								"nmap": {"route": {Command: &Command{
									Name: "switch new routing", Protocol: "ssh",
									Scr4args: &JsonConverter{Lang: "js", Script: ``},
									Msg: &util.Msg{
										Name: "ssh",
										Req:  json.RawMessage(`{}`),
										Rsp:  json.RawMessage(`{}`),
									},
								}}},
								"route": {"logout": {Command: &Command{
									Name: "save setting", Protocol: "ssh",
									Scr4args: &JsonConverter{Lang: "js", Script: ``},
									Msg: &util.Msg{
										Name: "ssh",
										Req:  json.RawMessage(`{}`),
										Rsp:  json.RawMessage(`{}`),
									},
								}}},
							},
						},
					},
					Next: []*RuleNode{},
				}, Archived: nil},
				// ref: https://thingsboard.io/docs/reference/snmp-api/
				// https://thingsboard.io/docs/user-guide/integrations/
				// https://thingsboard.io/docs/pe/user-guide/rule-engine-2-0/re-getting-started/
				{Id: 1903021818580123460, Name: "snmp setting", RuleNode: &RuleNode{
					Id:     1903021818580123450,
					Source: json.RawMessage(`{"get":"systemInfo","set":"lastUpdateTime"}`),
					Rpc4device: &Rpc4device{
						Where: &Command{
							Name: "is snmp device", Protocol: "snmpv2",
							Scr4args: &JsonConverter{Lang: "js", Script: `
// where('{"get": "systemInfo", "set": "lastUpdateTime"}', '{}')
// function where(evt, dev) {
  console.log("evt=", evt, "dev=", dev); //, String.fromCharCode.apply(String, msg));
  obj = JSON.parse(evt); //String.fromCharCode.apply(String, msg));
  return typeof obj.get !== 'undefined' && typeof obj.get !== 'undefined';
// }`},
						},
						Dfa4rpc: &Dfa4rpc{
							Protocol: "snmpv2", Init: &Command{
								Name: "snmp get", Protocol: "snmpv2",
								Scr4args: &JsonConverter{Lang: "js", Script: `
// adapt('{"get": "systemInfo", "set": "lastUpdateTime"}', '{}')
// function adapt(msg, dev) {
  console.log("msg=", msg, "dev=", dev);
  obj = JSON.parse(msg);
  console.log("msg obj.get=", obj.get, ", obj.set=", obj.set, "!");
  //var obj4rst = {method:"GET",params:{key:"OID"}};
  //obj4rst.params.key = obj.get;
  //return JSON.stringify(obj4rst);
  // return obj4rst
  obj.method = "GET";
  obj.params = {key:obj.get};
  return JSON.stringify(obj);
// }`},
								Msg: &util.Msg{
									Name: "snmp",
									Req:  json.RawMessage(`{"get":"systemInfo","set":"lastUpdateTime","method":"GET","params":{"key":"systemInfo"}}`),
									Rsp:  json.RawMessage(`{"get":"systemInfo","set":"lastUpdateTime","systemInfo":"SNMP device"}`),
								},
							},
						},
					},
					Next: []*RuleNode{
						{
							Rpc4device: &Rpc4device{
								Dfa4rpc: &Dfa4rpc{
									Protocol: "snmpv2", Init: &Command{
										Name: "snmp set", Protocol: "snmpv2",
										Scr4args: &JsonConverter{Lang: "js", Script: `
// adapt('{"get": "systemInfo", "set": "lastUpdateTime", "systemInfo": "SNMP device"}', '{}')
// function adapt(msg, dev) {
  console.log("msg=", msg, "dev=", dev);
  obj = JSON.parse(msg);
  console.log("msg obj.get=", obj.get, ", obj.set=", obj.set, "!");
  //var obj4rst = {method:"GET",params:{key:"OID"}};
  //obj4rst.params.key = obj.get;
  //return JSON.stringify(obj4rst);
  // return obj4rst
  obj.method = "SET";
  obj.params = {key:obj.set,value:Math.round(new Date().getTime()/1000)};
  return JSON.stringify(obj);
// }`},
										Msg: &util.Msg{
											Name: "snmp",
											Req:  json.RawMessage(`{"method":"SET","params":{"key":"lastUpdateTime","value":"12901200312"}}`),
											Rsp:  json.RawMessage(`null`),
										},
									},
								},
							},
						},
					},
				}, Archived: &util.FALSE},
			},
		},
	}
	byte4json, err := json.Marshal(dev)
	if err != nil {
		t.Errorf("json.Marshal(%v) err:%v", dev, err)
	}
	t.Logf("dev:\n%s\n!", util.String(byte4json))
}
