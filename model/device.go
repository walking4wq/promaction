package model

import (
	"encoding/json"
	"gitlab.com/walking4wq/promaction/util"
)

/*
ref:
https://thingsboard.io/docs/
https://thingsboard.io/docs/user-guide/entities-and-relations/

Devices - basic IoT entities that may produce telemetry data and handle RPC commands. For example, sensors, actuators, switches;
  Attributes - platform ability to assign custom key-value attributes to your entities (e.g configuration, data processing, visualization parameters).

Device Profiles - contains common settings for multiple devices: processing and transport configuration, etc. Each Device has the one and only profile at a single point in time.

Rule engine - covers data processing & actions on incoming telemetry and events.
Rule Node - processing units for incoming messages, entity lifecycle events, etc;
Rule Chain - defines the flow of the processing in the Rule Engine. May contain many rule nodes and links to other rule chains;

RPC - API and widgets to push commands from your apps and dashboards to devices and vice versa. only once connect/disconnect, and must in a short time/transaction
  Dfa - Deterministic finite automata(state machine) for coordinate the execution of multiple commands
    State - Flag the command prev/post execution
    Event - State change trigger event
      Command - Once Q&A between with server and device in RPC session

Runtime:
  Job - with cron msg
  Msg - union format of RPC request/response
*/

type Device struct {
	Code string `json:"code"`
	Name string `json:"name,omitempty"`
	//Secret        string `json:"secret,omitempty"`
	Token4refresh string `json:"token4refresh,omitempty"`
	//Token4access  string `json:"token4access,omitempty"`

	Attrs          `json:"attrs,omitempty"`
	*DeviceProfile `json:"prf,omitempty"`

	*History
}

type Rsp4device struct {
	Data []*Device `json:"data"`
	*Req4paging
}

type Attrs map[string]interface{}

type DeviceProfile struct {
	Id       int64    `json:"id"`
	Name     string   `json:"name,omitempty"`
	Attrs    util.Set `json:"attrs,omitempty"`
	Pri      int      `json:"pri,omitempty"` // priority level for discovery profile
	Archived *bool    `json:"arch,omitempty"`
	// protocol map to attrs for transport
	Transport map[string]Attrs `json:"transport,omitempty"` // dev attrs have high priority
	//Rpc       []*Rpc4device    `json:"rpc,omitempty"`
	Rules []*RuleChain `json:"rules,omitempty"`
}

type RuleChain struct {
	Id   int64  `json:"id"`
	Name string `json:"name,omitempty"`
	*RuleNode
	// Scr4out string    `json:"scr4out,omitempty"`
	Archived *bool `json:"arch,omitempty"`
}
type RuleNode struct {
	Id     int64           `json:"id"`
	Source json.RawMessage `json:"source,omitempty"`
	*Rpc4device
	prev []*RuleNode // `json:"-"`
	Next []*RuleNode `json:"next,omitempty"`
}
type Rpc4device struct { // should wrap by util.Job // it is an async rpc
	Id       int64     `json:"id"`
	Rpc      *util.Job `json:"rpc,omitempty"`
	Where    *Command  `json:"where,omitempty"`
	*Dfa4rpc `json:"dfa,omitempty"`
}
type Dfa4rpc struct {
	Protocol string   `json:"protocol,omitempty"`
	Init     *Command `json:"init,omitempty"`
	// ref: https://www.tutorialspoint.com/automata_theory/deterministic_finite_automaton.htm
	Trans map[string]*TransCmd `json:"trans,omitempty"` // it is private
	State map[string]*TransSta `json:"state,omitempty"` // it is private
}
type Command struct {
	Name           string         `json:"name,omitempty"`
	Protocol       string         `json:"protocol,omitempty"`
	Scr4args       *JsonConverter `json:"scr4args,omitempty"`
	*util.Msg      `json:"msg,omitempty"`
	*util.Services `json:"-"`
}
type Cmd4trans struct {
	*Command
	Dist int `json:"dist,omitempty"`
}
type TransCmd map[string]*Cmd4trans

type TransSta struct {
	Code string `json:"code"`
	Note string `json:"note,omitempty"`
}

type Rsp4dfaCmd struct {
	// nil is break, false is continued, true is finish
	Done *bool  `json:"done,omitempty"`
	Src  string `json:"src,omitempty"`
	Dst  string `json:"dst,omitempty"`
	Cur  string `json:"cur,omitempty"`
	Nxt  string `json:"nxt,omitempty"`
}

type JsonConverter struct {
	Lang   string `json:"lang,omitempty"` // default is JavaScript
	Script string `json:"script"`
}

func (this *JsonConverter) Adapt(in json.RawMessage) (out json.RawMessage, err error) {
	_ = in // TODO need implement
	return
}
