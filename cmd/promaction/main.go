package main

import (
	"fmt"
	"github.com/go-kit/log/level"
	log "github.com/inconshreveable/log15"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/promlog"
	promlogflag "github.com/prometheus/common/promlog/flag"
	"github.com/prometheus/common/version"
	toolkit_web "github.com/prometheus/exporter-toolkit/web"
	toolkit_webflag "github.com/prometheus/exporter-toolkit/web/kingpinflag"
	"gitlab.com/walking4wq/promaction/storage"
	"gitlab.com/walking4wq/promaction/web"
	"gitlab.com/walking4wq/promaction/web/svr"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var (
	appName = "promaction"
)

type flagConfig struct {
	url4storage string
	web         web.Options

	promlogConfig promlog.Config
}

func main() {
	fmt.Println("Hello PromAction(Prometheus AlertManager Notifier RPC Gateway)!")
	fmt.Println("args:" + strings.Join(os.Args[1:], ","))
	//flag.Parse()
	//fmt.Println("args parsed:" + strings.Join(flag.Args(), ","))
	//i := 0
	//flag.VisitAll(func(f *flag.Flag) {
	//	fmt.Printf("%d>%v\n", i, f)
	//	i++
	//})

	if os.Getenv("DEBUG") != "" {
		runtime.SetBlockProfileRate(20)
		runtime.SetMutexProfileFraction(20)
	}

	cfg := flagConfig{
		promlogConfig: promlog.Config{},
	}

	a := kingpin.New(filepath.Base(os.Args[0]), "The Promaction RPC gateway server").UsageWriter(os.Stdout)

	a.Version(version.Print(appName))

	a.HelpFlag.Short('h')
	a.Flag("url4storage", "Url for storage.").Default("redis://localhost:6379").StringVar(&cfg.url4storage)
	a.Flag("web.listen-address", "Address to listen on for UI, API, and telemetry.").
		Default("0.0.0.0:9527").StringVar(&cfg.web.ListenAddress)

	webConfig := toolkit_webflag.AddFlags(a)
	promlogflag.AddFlags(a, &cfg.promlogConfig)
	_, err := a.Parse(os.Args[1:])
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, fmt.Errorf("Error parsing commandline arguments: %w", err))
		a.Usage(os.Args[1:])
		os.Exit(1)
	}

	logger := promlog.New(&cfg.promlogConfig)
	_ = level.Info(logger).Log("info", "hi promaction")

	dir := "./log/"
	if _, err := ioutil.ReadDir(dir); err != nil {
		/*
			+-----+---+--------------------------+
			| rwx | 7 | Read, write and execute  |
			| rw- | 6 | Read, write              |
			| r-x | 5 | Read, and execute        |
			| r-- | 4 | Read,                    |
			| -wx | 3 | Write and execute        |
			| -w- | 2 | Write                    |
			| --x | 1 | Execute                  |
			| --- | 0 | no permissions           |
			+------------------------------------+

			+------------+------+-------+
			| Permission | Octal| Field |
			+------------+------+-------+
			| rwx------  | 0700 | User  |
			| ---rwx---  | 0070 | Group |
			| ------rwx  | 0007 | Other |
			+------------+------+-------+
		*/
		_ = os.Mkdir(dir, 0777) // 0666)
	}
	now := time.Now().Local().Format("20060102") // ("2006-01-02 15:04:05.000")
	reset4log(dir, now, cfg.promlogConfig.Level.String())
	go func() {
		defer func() {
			log.Crit("quit log rotate")
			os.Exit(10)
		}()
		now4prev := ""
		for {
			now = time.Now().Local().Format("20060102") // ("2006-01-02 15:04:05.000")
			if now4prev == now {
				time.Sleep(time.Minute * 5)
				continue
			}
			now4prev = now
			reset4log(dir, now, cfg.promlogConfig.Level.String())
		}
	}()
	// startup pprof: import _ "net/http/pprof"
	go func() {
		addr := "0.0.0.0:6060"
		log.Info("start pprof goroutines", "addr", addr)
		if err := http.ListenAndServe(addr, nil); err != nil {
			log.Crit("start pprof failure", "addr", addr, "err", err)
			os.Exit(11)
		}
	}()
	// startup prometheus metrics
	go func() {
		addr := ":2112"
		http.Handle("/metrics", promhttp.Handler())
		if err := http.ListenAndServe(addr, nil); err != nil {
			log.Crit("start prometheus", "addr", addr, "err", err)
			os.Exit(12)
		}
	}()

	var stg storage.Storage
	stg, err = storage.New(cfg.url4storage)
	if err != nil {
		log.Crit("New storage", "url", cfg.url4storage, "err", err)
		os.Exit(2)
	}
	cfg.web.Storage = stg

	err = toolkit_web.Validate(*webConfig)
	if err != nil {
		// level.Error(logger).Log("msg", "Unable to validate web configuration file", "err", err)
		log.Crit("Unable to validate web configuration file", "err", err)
		os.Exit(10)
	}
	hdl := web.New(&cfg.web)
	err = hdl.Run(*webConfig)
	if err != nil {
		log.Crit("Unable to run web server", "err", err)
		os.Exit(11)
	}
	// hdl.Wait()
	qq4svr, err := svr.Start(&cfg.web)
	if err != nil {
		log.Crit("Unable to run server", "err", err)
		os.Exit(20)
	}

	select {
	case <-hdl.Quit():
		log.Info("web server quit")
	case <-qq4svr:
		log.Info("server quit")
	}
}

func reset4log(dir, now, lvl string) {
	path := fmt.Sprintf("%spromaction_%s.log", dir, now)
	h := log.CallerStackHandler("%+v", log.MultiHandler( // log.FailoverHandler(
		// D:/coding/ztesoft/blockchain/ethereum/geth/go_get_github/src/github.com/ethereum/go-ethereum/log/handler.go:222
		// log.Must.NetHandler("tcp", ":9090", log.JsonFormat()),
		log.Must.FileHandler(path, log.LogfmtFormat()), // LogfmtFormat
		log.StdoutHandler)) // format ref from:https://github.com/go-stack/stack/blob/master/stack.go
	// log.Root().SetHandler(log.LvlFilterHandler(log.LvlTrace, h))
	if lvl4log, err := log.LvlFromString(lvl); err != nil {
		log.Root().SetHandler(log.LvlFilterHandler(log.LvlInfo, h)) // log.LvlDebug, h))
	} else {
		log.Root().SetHandler(log.LvlFilterHandler(lvl4log, h))
	}
}
